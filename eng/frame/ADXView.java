package frame;

/**
 * A on-screen view of a portion of the game world. Multiples views can be added to achieve a split-screen effect.
 * On-screen coordinates specify the position the view will be drawn on the canvas.
 * In-world coordinates specify the position of the view in the game world,
 * or the portion of the game world that will then be rendered at the on-screen position.
 * @author Alexandre Desbiens
 */
public class ADXView {
	
	private int x;
	private int y;
	private int screenX = 0;
	private int screenY = 0;
	private int width = 0;
	private int height = 0;
	private int id = -1;
	
	/**
	 * Creates a view with the given coordinates on screen.
	 * @param x X position of the view on screen, in pixels
	 * @param y Y position of the view on screen, in pixels
	 * @param width Width of the view, in pixels
	 * @param height Height of the view, in pixels
	 */
	public ADXView(int x, int y, int width, int height) {
		this.screenX = x;
		this.screenY = y;
		this.width = width;
		this.height = height;
	}
	
	/**
	 * Returns the ID of the view, which is its position in the view list.
	 * @return ID of the view
	 */
	public int getID() {
		return id;
	}
	
	/**
	 * Sets the ID of the view.
	 * Should only be used by the engine.
	 * @param id
	 */
	public void setID(int id) {
		this.id = id;
	}
	
	/**
	 * Returns the X position of the view in the game world.
	 * @return World X position, in pixels
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Sets the X position of the view in the game world.
	 * @param x World X position, in pixels
	 */
	public void setX(int x) {
		this.x = x;
	}
	
	/**
	 * Returns the Y position of the view in the game world.
	 * @return World Y position, in pixels
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Sets the Y position of the view in the game world.
	 * @param y World Y position, in pixels
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * Returns the X position of the view on screen.
	 * @return Screen X position, in pixels
	 */
	public int getScreenX() {
		return screenX;
	}
	
	/**
	 * Sets the X position of the view on screen.
	 * @param x Screen X position, in pixels
	 */
	public void setScreenX(int x) {
		this.screenX = x;
	}
	
	/**
	 * Returns the Y position of the view on screen.
	 * @return Screen Y position, in pixels
	 */
	public int getScreenY() {
		return screenY;
	}
	
	/**
	 * Sets the Y position of the view on screen.
	 * @param y Screen Y position, in pixels
	 */
	public void setScreenY(int y) {
		this.screenY = y;
	}
	
	/**
	 * Returns the width of the view.
	 * @return View width, in pixels
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * Sets the width of the view.
	 * @param width View width, in pixels
	 */
	public void setWidth(int width) {
		this.width = width;
	}
	
	/**
	 * Returns the height of the view
	 * @return View height, in pixels
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Sets the height of the view.
	 * @param height View height, in pixels
	 */
	public void setHeight(int height) {
		this.height = height;
	}

}
