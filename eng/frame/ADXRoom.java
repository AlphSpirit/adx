package frame;

import io.ADXInput;
import render.ADXGraphics;

/**
 * The abstract room class that every room will extend from.
 * A room is a state of the game and manages the creation of all instances or components needed for that state.
 * @author Alexandre Desbiens
 */
public abstract class ADXRoom {
	
	private int roomID;
	
	/**
	 * Adds the room to the game's rooms list.
	 * @param roomID ID of the room
	 */
	public ADXRoom(int roomID) {
		this.roomID = roomID;
	}
	
	/**
	 * Instance initialization function, where all loading code should be placed.
	 * @param game Pointer to the game instance
	 */
	public abstract void init(ADXGame game);
	/**
	 * Instance update function, where all logic code should be placed.
	 * @param game Pointer to the game instance
	 * @param input Input object
	 */
	public abstract void update(ADXGame game, ADXInput input);
	/**
	 * Instance render function, where all drawing related code should be placed.
	 * @param game Pointer to the game instance
	 * @param g Graphics object
	 */
	public abstract void render(ADXGame game, ADXGraphics g);
	
	/**
	 * Called when the room is exited or the game is closed.
	 * @param game Pointer to the game instance
	 * @param nextRoom Pointer to the next room instance, or null if there is no next room
	 */
	public void exit(ADXGame game, ADXRoom nextRoom) {
	}
	
	/**
	 * Called when the game texture is resized, to allow the room to reposition components.
	 * @param game Pointer to the game instance
	 */
	public void resize(ADXGame game) {
	}
	
	/**
	 * Returns the ID of the room.
	 * @return ID of the room
	 */
	public int getRoomID() {
		return roomID;
	}

}
