package frame;

import io.ADXInput;
import render.ADXGraphics;

/**
 * The abstract instance class that every game instance will extend from.
 * Each game instance will be added to the game's instance list and will then be updated and rendered automatically.
 * @author Alexandre Desbiens
 */
public abstract class ADXInstance {
	
	private int roomID;
	private int depth;
	
	/** Empty contructor, will initialize the instance with a depth of 0 and the room ID of the current room. */
	public ADXInstance() {
		this.roomID = Integer.MIN_VALUE;
		this.depth = 0;
	}
	
	/**
	 * Creates an instance in the given room, while the default value of 0 will be assigned to the depth.
	 * @param roomID Id of the room to add the instance in
	 */
	public ADXInstance(int roomID) {
		this.roomID = roomID;
		this.depth = 0;
	}
	
	/**
	 * Creates an instance in the given room with the given depth.
	 * @param roomID ID of the room to add the instance in
	 * @param depth Depth of the instance
	 */
	public ADXInstance(int roomID, int depth) {
		this.roomID = roomID;
		this.depth = depth;
	}
	
	/**
	 * Returns the room ID of the instance.
	 * @return Room ID of the instance
	 */
	public int getRoomID() {
		return roomID;
	}
	
	/**
	 * Sets the roomID of the instance to the given one.
	 * @param id Room ID to set to the instance
	 */
	public void setRoomID(int id) {
		roomID = id;
	}
	
	/**
	 * Returns the depth of the instance.
	 * @return Depth of the instance
	 */
	public int getDepth() {
		return depth;
	}
	
	/**
	 * Sets the depth of the instance.
	 * Should only be used by the engine. Use the {@link #changeDepth(ADXGame, int) changeDepth} method instead.
	 * @param depth
	 */
	public void setDepth(int depth) {
		this.depth = depth;
	}
	
	/**
	 * Instance initialization function, where all loading code should be placed.
	 * @param game Pointer to the game instance
	 */
	public abstract void init(ADXGame game);
	/**
	 * Instance update function, where all logic code should be placed.
	 * @param game Pointer to the game instance
	 * @param input Input object
	 */
	public abstract void update(ADXGame game, ADXInput input);
	/**
	 * Instance render function, where all drawing related code should be placed.
	 * @param game Pointer to the game instance
	 * @param g Graphics object
	 */
	public abstract void render(ADXGame game, ADXGraphics g);
	
	/**
	 * Deletes the object by removing it from the instance list.
	 * @param game Pointer to the game instance
	 */
	public void delete(ADXGame game) {
		game.removeInstance(this);
	}
	
	/**
	 * Orders the game to change the depth and position in the instance list of this instance.
	 * @param game Pointer to the game instance
	 * @param depth New depth of the instance
	 */
	public void changeDepth(ADXGame game, int depth) {
		game.changeInstanceDepth(this, depth);
	}

}
