package tools.mapEditor;

import io.ADXTextFile;

import java.util.ArrayList;

public abstract class ADXMapLoader {
	
	public ADXMapLoader(String path) {
		
		ADXTextFile file = new ADXTextFile(path);
		String line;
		String[] split;
		int layer;
		int index = 0;
		int assetCode = 0;
		while ((line = file.readLine()) != null) {
			index = 0;
			split = line.split(";");
			layer = Integer.parseInt(split[index++]);
			while (index < split.length) {
				assetCode = Integer.parseInt(split[index++]);
				placeAsset(assetCode, layer, Integer.parseInt(split[index++]), Integer.parseInt(split[index++]), null);
			}
		}
		
	}
	
	public abstract void placeAsset(int id, int layer, int x, int y, ArrayList<Object> arguments);

}
