package shapes;

/**
 * Simple Circle structure made of a set of coordinates and a radius.
 * The X and Y positions always refer to the center of the circle.
 * @author Alexandre Desbiens
 */
public class ADXCircle extends ADXShape {

	private float x;
	private float y;
	private float radius;

	/**
	 * Initialize the structure with the given position and radius.
	 * @param x X position
	 * @param y Y position
	 * @param radius Circle radius
	 */
	public ADXCircle(float x, float y, float radius) {
		super(ADXShape.SHAPE_CIRCLE);
		this.x = x;
		this.y = y;
		this.radius = radius;
	}

	/**
	 * Returns the X position of the circle.
	 * @return X position
	 */
	public float getX() {
		return x;
	}

	/**
	 * Sets the X position of the circle.
	 * @param x X position
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * Returns the Y position of the circle structure.
	 * @return Y position
	 */
	public float getY() {
		return y;
	}

	/**
	 * Sets the Y position of the circle.
	 * @param y Y position
	 */
	public void setY(float y) {
		this.y = y;
	}

	/**
	 * Gets the radius of the circle structure.
	 * @return Circle radius
	 */
	public float getRadius() {
		return radius;
	}

	/**
	 * Sets the radius of the circle.
	 * @param radius Circle radius
	 */
	public void setRadius(float radius) {
		this.radius = radius;
	}

	/**
	 * Tests a collision with a given point.
	 * @param p Point to collide with
	 * @return Collision flag
	 */
	public boolean collides(ADXPoint p) {
		return ADXCollision.checkCollision(p, this);
	}
	
	/**
	 * Tests a collision with a given rectangle.
	 * @param r Rectangle to collide with
	 * @return Collision flag
	 */
	public boolean collides(ADXRectangle r) {
		return ADXCollision.checkCollision(r, this);
	}

	/**
	 * Tests a collision with another circle.
	 * @param c Circle to collide with
	 * @return Collision flag
	 */
	public boolean collides(ADXCircle c) {
		return ADXCollision.checkCollision(this, c);
	}
	
	public boolean collides(ADXShape s) {
		return ADXCollision.checkCollision((ADXShape) this, s);
	}

}
