package shapes;

import java.util.ArrayList;

public abstract class ADXShape {
	
	public static final byte SHAPE_POINT = 1;
	public static final byte SHAPE_RECTANGLE = 2;
	public static final byte SHAPE_CIRCLE = 3;
	public static final byte SHAPE_LINE = 4;
	public static final byte SHAPE_POLYGON = 5;
	
	private byte type;
	
	public ADXShape(byte type) {
		this.type = type;
	}
	
	public byte getType() {
		return type;
	}
	
	public boolean collides(ArrayList<ADXCollidableInstance> lI) {
		for (ADXCollidableInstance i : lI) {
			if (ADXCollision.checkCollision(this, i.getCollisionShape())) {
				return true;
			}
		}
		return false;
	}

}
