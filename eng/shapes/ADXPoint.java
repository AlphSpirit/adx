package shapes;

/**
 * Simple point structure made of one set of coordinates.
 * @author Alexandre Desbiens
 */
public class ADXPoint extends ADXShape {

	private float x;
	private float y;

	/**
	 * Initializes the point structure with the given position.
	 * @param x X position
	 * @param y Y position
	 */
	public ADXPoint(float x, float y) {
		super(ADXShape.SHAPE_POINT);
		this.x = x;
		this.y = y;
	}

	/**
	 * Returns the X position of the point.
	 * @return X position
	 */
	public float getX() {
		return x;
	}

	/**
	 * Sets the X position of the point.
	 * @param x X position
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * Returns the Y position of the point structure.
	 * @return Y position
	 */
	public float getY() {
		return y;
	}

	/**
	 * Sets the Y position of the point.
	 * @param y Y position
	 */
	public void setY(float y) {
		this.y = y;
	}

	/**
	 * Tests a collision with another point.
	 * @param p Point to collide with
	 * @return Collision flag
	 */
	public boolean collides(ADXPoint p) {
		return ADXCollision.checkCollision(this, p);
	}

	/**
	 * Tests a collision with a given rectangle.
	 * @param r Rectangle to collide with
	 * @return Collision flag
	 */
	public boolean collides(ADXRectangle r) {
		return ADXCollision.checkCollision(this, r);
	}

	/**
	 * Tests a collision with a given circle.
	 * @param c Circle to collide with
	 * @return Collision flag
	 */
	public boolean collides(ADXCircle c) {
		return ADXCollision.checkCollision(this, c);
	}
	
	/**
	 * Tests a collision with a given polygon.
	 * @param p Polygon to collide with
	 * @return Collision flag
	 */
	public boolean collides(ADXPolygon p) {
		return ADXCollision.checkCollision(this, p);
	}
	
	public boolean collides(ADXShape s) {
		return ADXCollision.checkCollision((ADXShape) this, s);
	}

}
