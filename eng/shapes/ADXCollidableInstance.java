package shapes;

import io.ADXInput;
import render.ADXGraphics;
import frame.ADXGame;
import frame.ADXInstance;

public abstract class ADXCollidableInstance extends ADXInstance {
	
	protected ADXShape collisionShape = null;

	@Override
	public abstract void init(ADXGame game);

	@Override
	public abstract void update(ADXGame game, ADXInput input);

	@Override
	public abstract void render(ADXGame game, ADXGraphics g);
	
	public ADXShape getCollisionShape() {
		return collisionShape;
	}

}
