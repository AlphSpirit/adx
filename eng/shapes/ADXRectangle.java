package shapes;

/**
 * Simple rectangle structure made of a set of coordinates and a dimension (width and height).
 * The X and Y positions will always be in the top left corner of the rectangle.
 * @author Alexandre Desbiens
 */
public class ADXRectangle extends ADXShape {

	private float x;
	private float y;
	private float width;
	private float height;

	/**
	 * Initializes the rectangle structure with the given position and size.
	 * @param x X position
	 * @param y Y position
	 * @param width Rectangle width
	 * @param height Rectangle height
	 */
	public ADXRectangle(float x, float y, float width, float height) {
		super(ADXShape.SHAPE_RECTANGLE);
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	/**
	 * Initializes the structure by creating the smallest rectangle that contains the two given points.
	 * @param p1 First point to contain
	 * @param p2 Second point to contain
	 */
	public ADXRectangle(ADXPoint p1, ADXPoint p2) {
		super(ADXShape.SHAPE_RECTANGLE);
		if (p1.getX() < p2.getX()) {
			x = p1.getX();
		} else {
			x = p2.getX();
		}
		if (p1.getY() < p2.getY()) {
			y = p1.getY();
		} else {
			y = p2.getY();
		}
		width = Math.abs(p1.getX() - p2.getX());
		height = Math.abs(p1.getY() - p2.getY());
	}

	/**
	 * Returns the X position of the rectangle.
	 * @return X position
	 */
	public float getX() {
		return x;
	}

	/**
	 * Sets the X position of the rectangle.
	 * @param x X position
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * Returns the Y position of the rectangle structure.
	 * @return Y position
	 */
	public float getY() {
		return y;
	}

	/**
	 * Sets the Y position of the rectangle.
	 * @param y Y position
	 */
	public void setY(float y) {
		this.y = y;
	}

	/**
	 * Returns the width of the rectangle.
	 * @return Rectangle width
	 */
	public float getWidth() {
		return width;
	}

	/**
	 * Sets the width of the rectangle.
	 * @param width Rectangle width
	 */
	public void setWidth(float width) {
		this.width = width;
	}

	/**
	 * Returns the height of the rectangle.
	 * @return Rectangle height
	 */
	public float getHeight() {
		return height;
	}

	/**
	 * Sets the height of the rectangle.
	 * @param height Rectangle height
	 */
	public void setHeight(float height) {
		this.height = height;
	}
	
	/**
	 * Tests a collision with a given point.
	 * @param p Point to collide with
	 * @return Collision flag
	 */
	public boolean collides(ADXPoint p) {
		return ADXCollision.checkCollision(p, this);
	}

	/**
	 * Tests a collision with another rectangle.
	 * @param r Rectangle to collide with
	 * @return Collision flag
	 */
	public boolean collides(ADXRectangle r) {
		return ADXCollision.checkCollision(this, r);
	}
	
	/**
	 * Tests a collision with a given circle.
	 * @param c Circle to collide with
	 * @return Collision flag
	 */
	public boolean collides(ADXCircle c) {
		return ADXCollision.checkCollision(this, c);
	}
	
	public boolean collides(ADXShape s) {
		return ADXCollision.checkCollision((ADXShape) this, s);
	}

}
