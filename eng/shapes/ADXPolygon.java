package shapes;

/**
 * Simple polygon structure made of an array of coordinates.
 * @author Alexandre Desbiens
 */
public class ADXPolygon extends ADXShape {
	
	private int vertices = -1;
	private float[] x = null;
	private float[] y = null;
	
	/**
	 * Initializes the structure with 2 arrays of X and Y positions.
	 * @param x X positions
	 * @param y Y positions
	 */
	public ADXPolygon(float[] x, float[] y) {
		super(ADXShape.SHAPE_POLYGON);
		if (x.length != y.length || x.length == 0) {
			return;
		}
		this.x = x;
		this.y = y;
		vertices = x.length;
	}
	
	/**
	 * Creates the structure with a list of float.
	 * The structure of the points must be as follow: x1, y1, x2, y2, ...
	 * This list must be even, for there must be an equal number of X and Y positions.
	 * @param p Positions list
	 */
	public ADXPolygon(float... p) {
		super(ADXShape.SHAPE_POLYGON);
		if (p.length % 2 != 0) {
			return;
		}
		vertices = p.length / 2;
		x = new float[vertices];
		y = new float[vertices];
		for (int i = 0; i < vertices; i++) {
			x[i] = p[i * 2];
			y[i] = p[i * 2 + 1];
		}
	}
	
	/**
	 * Returns the number of vertices of the polygon
	 * @return Number of vertices
	 */
	public int getVertices() {
		return vertices;
	}
	
	/**
	 * Returns the array of X positions.
	 * @return X positions
	 */
	public float[] getX() {
		return x;
	}
	
	/**
	 * Returns the array of Y positions.
	 * @return Y positions
	 */
	public float[] getY() {
		return y;
	}
	
	/**
	 * Tests the collision with a give point.
	 * @param p Point to collide with
	 * @return Collision flag
	 */
	public boolean collides(ADXPoint p) {
		return ADXCollision.checkCollision(p, this);
	}
	
	public boolean collides(ADXShape s) {
		return ADXCollision.checkCollision((ADXShape) this, s);
	}

}
