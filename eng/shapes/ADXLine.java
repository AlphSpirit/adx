package shapes;

/**
 * Simple line structure made of 2 sets of coordinates.
 * @author Alexandre Desbiens
 */
public class ADXLine extends ADXShape {
	
	private float x1;
	private float y1;
	private float x2;
	private float y2;
	
	/**
	 * Initializes the structure with 2 sets of coordinates.
	 * @param x1 X1 position
	 * @param y1 Y1 position
	 * @param x2 X2 position
	 * @param y2 Y2 position
	 */
	public ADXLine(float x1, float y1, float x2, float y2) {
		super(ADXShape.SHAPE_LINE);
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
	
	/**
	 * Returns the X position of the first point.
	 * @return X1 position
	 */
	public float getX1() {
		return x1;
	}
	
	/**
	 * Sets the X position of the first point to the given value.
	 * @param x X1 position
	 */
	public void setX1(float x) {
		x1 = x;
	}
	
	/**
	 * Returns the Y position of the first point.
	 * @return Y1 position
	 */
	public float getY1() {
		return y1;
	}
	
	/**
	 * Sets the Y position of the first point to the given value.
	 * @param y Y1 position
	 */
	public void setY1(float y) {
		y1 = y;
	}
	
	/**
	 * Returns the X position of the second point.
	 * @return X2 position
	 */
	public float getX2() {
		return x2;
	}
	
	/**
	 * Sets the X position of the second point to the given value.
	 * @param x X2 position
	 */
	public void setX2(float x) {
		x2 = x;
	}
	
	/**
	 * Returns the Y position of the second point.
	 * @return Y2 position
	 */
	public float getY2() {
		return y2;
	}
	
	/**
	 * Sets the Y position of the second point to the given value.
	 * @param y Y2 position
	 */
	public void setY2(float y) {
		y2 = y;
	}
	
	/**
	 * Tests a collision with another line.
	 * @param l Line to collide with
	 * @return Collision flag
	 */
	public boolean collides(ADXLine l) {
		return ADXCollision.checkCollision(this, l);
	}
	
	/**
	 * Retrieves the collision point between 2 lines.
	 * @param l Line to collide with
	 * @return Collision point, or null if there is no collision
	 */
	public ADXPoint getCollision(ADXLine l) {
		return ADXCollision.getCollision(this, l);
	}
	
	public boolean collides(ADXShape s) {
		return ADXCollision.checkCollision((ADXShape) this, s);
	}

}
