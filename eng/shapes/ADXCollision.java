package shapes;

import math.ADXMath;

/**
 * Collision testing abstract class.
 * This class gives a good number of collision functions that can test if 2 simple shapes collides.
 * @author Alexandre Desbiens
 */
public abstract class ADXCollision {
	
	public static boolean checkCollision(ADXShape s1, ADXShape s2) {
		switch (s1.getType()) {
		case ADXShape.SHAPE_POINT:
			switch(s2.getType()) {
			case ADXShape.SHAPE_POINT:
				return checkCollision((ADXPoint) s1, (ADXPoint) s2);
			case ADXShape.SHAPE_RECTANGLE:
				return checkCollision((ADXPoint) s1, (ADXRectangle) s2);
			case ADXShape.SHAPE_CIRCLE:
				return checkCollision((ADXPoint) s1, (ADXCircle) s2);
			case ADXShape.SHAPE_POLYGON:
				return checkCollision((ADXPoint) s1, (ADXPolygon) s2);
			}
			break;
		case ADXShape.SHAPE_RECTANGLE:
			switch(s2.getType()) {
			case ADXShape.SHAPE_POINT:
				return checkCollision((ADXPoint) s2, (ADXRectangle) s1);
			case ADXShape.SHAPE_RECTANGLE:
				return checkCollision((ADXRectangle) s1, (ADXRectangle) s2);
			case ADXShape.SHAPE_CIRCLE:
				return checkCollision((ADXRectangle) s1, (ADXCircle) s2);
			}
			break;
		case ADXShape.SHAPE_CIRCLE:
			switch(s2.getType()) {
			case ADXShape.SHAPE_POINT:
				return checkCollision((ADXPoint) s2, (ADXCircle) s1);
			case ADXShape.SHAPE_RECTANGLE:
				return checkCollision((ADXRectangle) s2, (ADXCircle) s1);
			case ADXShape.SHAPE_CIRCLE:
				return checkCollision((ADXCircle) s1, (ADXCircle) s2);
			}
			break;
		case ADXShape.SHAPE_LINE:
			switch(s2.getType()) {
			case ADXShape.SHAPE_LINE:
				return checkCollision((ADXLine) s1, (ADXLine) s2);
			}
			break;
		case ADXShape.SHAPE_POLYGON:
			switch(s2.getType()) {
			case ADXShape.SHAPE_POINT:
				return checkCollision((ADXPoint) s2, (ADXPolygon) s1);
			}
			break;
		}
		return false;
	}

	/**
	 * Tests the collision between 2 points.
	 * @param p1 Point 1 to collide
	 * @param p2 Point 2 to collide
	 * @return True if the 2 points collide
	 */
	public static boolean checkCollision(ADXPoint p1, ADXPoint p2) {
		return p1.getX() == p2.getX() && p1.getY() == p2.getY();
	}

	/**
	 * Tests the collision between a point and a rectangle.
	 * @param p Point to collide
	 * @param r Rectangle to collide
	 * @return True if the point and the rectangle collide
	 */
	public static boolean checkCollision(ADXPoint p, ADXRectangle r) {
		if (p.getX() < r.getX() || p.getY() < r.getY() || p.getX() > r.getX() + r.getWidth() || p.getY() > r.getY() + r.getHeight()) {
			return false;
		}
		return true;
	}

	/**
	 * Tests the collision between a point and a circle.
	 * @param p Point to collide
	 * @param c Circle to collide
	 * @return True if the point and the circle collide
	 */
	public static boolean checkCollision(ADXPoint p, ADXCircle c) {
		return ADXMath.dist(p.getX(), p.getY(), c.getX(), c.getY()) <= c.getRadius();
	}

	/**
	 * Tests the collision between a point and a complex polygon.
	 * @param p Point to collide
	 * @param o Polygon to collide
	 * @return True if the point and the polygon collide
	 */
	public static boolean checkCollision(ADXPoint p, ADXPolygon o) {
		boolean c = false;
		int i = 0;
		int j = 0;
		for (i = 0, j = o.getVertices() - 1; i < o.getVertices(); j = i++) {
			if (((o.getY()[i] > p.getY()) != (o.getY()[j] > p.getY())) &&
					(p.getX() < (o.getX()[j] - o.getX()[i]) * (p.getY() - o.getY()[i]) / (o.getY()[j] - o.getY()[i]) + o.getX()[i])) {
				c = !c;
			}
		}
		return c;
	}

	/**
	 * Tests the collision between 2 rectangles.
	 * @param r1 Rectangle 1 to collide
	 * @param r2 Rectangle 2 to collide
	 * @return True if the 2 rectangles collide
	 */
	public static boolean checkCollision(ADXRectangle r1, ADXRectangle r2) {
		if (r1.getX() + r1.getWidth() <= r2.getX() || r1.getY() + r1.getHeight() <= r2.getY() || r1.getX() >= r2.getX() + r2.getWidth()
				|| r1.getY() >= r2.getY() + r2.getHeight()) {
			return false;
		}
		return true;
	}

	/**
	 * Tests the collision between a rectangle and a circle.
	 * @param r Rectangle to collide
	 * @param c Circle to collide
	 * @return True if the rectangle and the circle collide
	 */
	public static boolean checkCollision(ADXRectangle r, ADXCircle c) {
		if (checkCollision(new ADXPoint(c.getX(), c.getY()), r)) {
			return true;
		}
		float x = ADXMath.clamp(c.getX(), r.getX(), r.getX() + r.getWidth());
		float y = ADXMath.clamp(c.getY(), r.getY(), r.getY() + r.getHeight());
		if (ADXMath.dist(x, y, c.getX(), c.getY()) <= c.getRadius()) {
			return true;
		}
		return false;
	}

	/**
	 * Tests the collision between 2 circles.
	 * @param c1 Circle 1 to collide
	 * @param c2 Circle 2 to collide
	 * @return True if the 2 circles collide
	 */
	public static boolean checkCollision(ADXCircle c1, ADXCircle c2) {
		return ADXMath.dist(c1.getX(), c1.getY(), c2.getX(), c2.getY()) <= c1.getRadius() + c2.getRadius();
	}

	/**
	 * Tests the collision between 2 lines.
	 * @param l1 Line 1 to collide
	 * @param l2 Line 2 to collide
	 * @return True if the 2 lines collide
	 */
	public static boolean checkCollision(ADXLine l1, ADXLine l2) {
		return getCollision(l1, l2) != null;
	}

	/**
	 * Retrieves the collision point between 2 lines.
	 * @param l1 Line 1 to collide
	 * @param l2 Line 2 to collide
	 * @return Collision point, or null if there is no collision
	 */
	public static ADXPoint getCollision(ADXLine l1, ADXLine l2) {
		// Start by testing the two rectangles around the lines for optimization
		ADXRectangle r1 = new ADXRectangle(new ADXPoint(l1.getX1(), l1.getY1()), new ADXPoint(l1.getX2(), l1.getY2()));
		ADXRectangle r2 = new ADXRectangle(new ADXPoint(l2.getX1(), l2.getY1()), new ADXPoint(l2.getX2(), l2.getY2()));
		if (!checkCollision(r1, r2)) {
			return null;
		}
		// Calculates the repeating bits
		float a = l1.getX1() * l1.getY2() - l1.getY1() * l1.getX2();
		float b = l2.getX1() * l2.getY2() - l2.getY1() * l2.getX2();
		float c = (l1.getX1() - l1.getX2()) * (l2.getY1() - l2.getY2()) - (l1.getY1() - l1.getY2()) * (l2.getX1() - l2.getX2());
		// If the denominator is 0, the lines are parallel. This case is not
		// handled.
		if (c == 0) {
			return null;
		}
		// Find the collision point between the lines
		float x = (a * (l2.getX1() - l2.getX2()) - (l1.getX1() - l1.getX2()) * b) / c;
		float y = (a * (l2.getY1() - l2.getY2()) - (l1.getY1() - l1.getY2()) * b) / c;
		ADXPoint p = new ADXPoint(x, y);
		// Verify that the point lies within the segments
		if (checkCollision(p, r1) && checkCollision(p, r2)) {
			return p;
		} else {
			return null;
		}
	}

}
