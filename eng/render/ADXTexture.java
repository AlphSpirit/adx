package render;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.lwjgl.BufferUtils;
import org.newdawn.slick.Color;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.EXTFramebufferObject.*;

public class ADXTexture {

	private int textureID;
	private ByteBuffer textureBuffer;
	private int fboID;

	private int width;
	private int height;

	public ADXTexture(int width, int height) {

		// Giving attributes
		this.width = width;
		this.height = height;
		// Creating texture
		textureID = glGenTextures();
		textureBuffer = BufferUtils.createByteBuffer(width * height * 4);
		textureBuffer.order(ByteOrder.nativeOrder());
		// Filling texture
		for (int i = 0; i < width * height; i++) {
			textureBuffer.put((byte) 0);
			textureBuffer.put((byte) 0);
			textureBuffer.put((byte) 0);
			textureBuffer.put((byte) 0);
		}
		// Creating image
		textureBuffer.flip();
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureBuffer);
		// Creating FBO
		fboID = glGenFramebuffersEXT();
		// Attaching texture
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fboID);
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, textureID, 0);
		// Binding the default FBO
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	}

	/* Accessors/Mutators */

	public int getTextureID() {
		return textureID;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	/* Functions */

	public void bind(boolean clear) {
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fboID);
		if (clear) {
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}
	}
	
	public void fillPixelBuffer() {
		glBindTexture(GL_TEXTURE_2D, textureID);
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureBuffer);
	}

	public Color getPixelColor(int x, int y) {
		int r = textureBuffer.get((x + y * width) * 4);
		if (r < 0) {
			r = 256 + r;
		}
		int g = textureBuffer.get((x + y * width) * 4 + 1);
		if (g < 0) {
			g = 256 + g;
		}
		int b = textureBuffer.get((x + y * width) * 4 + 2);
		if (b < 0) {
			b = 256 + b;
		}
		int a = textureBuffer.get((x + y * width) * 4 + 3);
		if (a < 0) {
			a = 256 + a;
		}
		return new Color(r, g, b, a);
	}

	public void destroy() {
		textureBuffer.clear();
		glDeleteTextures(textureID);
		glDeleteFramebuffersEXT(textureID);
	}

}