package render;

import java.io.IOException;
import java.util.HashMap;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import frame.ADXGame;

public class ADXSprite {

	private static HashMap<String, Texture> mapTexture = new HashMap<String, Texture>();

	private Texture tex;
	private int width;
	private int height;
	private int originX = 0;
	private int originY = 0;
	private int imagesX;
	private int imagesY;
	private float imageWidth;
	private float imageHeight;
	private byte[] pixels = null;

	public ADXSprite(String path) {
		this(path, 1, 1);
	}

	public ADXSprite(String path, int imagesX, int imagesY) {
		tex = loadTexture(path);
		width = tex.getImageWidth() / imagesX;
		height = tex.getImageHeight() / imagesY;
		this.imagesX = imagesX;
		this.imagesY = imagesY;
		imageWidth = width / (float) tex.getTextureWidth();
		imageHeight = height / (float) tex.getTextureHeight();
	}
	
	public static Texture loadTexture(String path) {
		Texture tex;
		if (!mapTexture.containsKey(path)) {
			ADXGame.postString("Loading texture " + path + "...");
			try {
				tex = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(path), GL11.GL_NEAREST);
			} catch (IOException e) {
				ADXGame.postWarning("Error while loading texture " + path);
				return null;
			}
			mapTexture.put(path, tex);
			return tex;
		} else {
			tex = mapTexture.get(path);
			return tex;
		}
	}

	public void setOrigin(int x, int y) {
		originX = x;
		originY = y;
	}

	public Texture getTexture() {
		return tex;
	}

	public int getTextureID() {
		return tex.getTextureID();
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getTotalWidth() {
		return width * imagesX;
	}

	public int getTotalHeight() {
		return height * imagesY;
	}

	public int getImagesX() {
		return imagesX;
	}

	public int getImagesY() {
		return imagesY;
	}

	public float getImageWidth() {
		return imageWidth;
	}

	public float getImageHeight() {
		return imageHeight;
	}

	public int getOriginX() {
		return originX;
	}

	public int getOriginY() {
		return originY;
	}
	
	public Color getPixelColor(int x, int y) {
		if (pixels == null) {
			pixels = tex.getTextureData();
		}
		int mul;
		if (tex.hasAlpha()) {
			mul = 4;
		} else {
			mul = 3;
		}
		int r = pixels[(x + y * width) * mul];
		if (r < 0) {
			r = 256 + r;
		}
		int g = pixels[(x + y * width) * mul + 1];
		if (g < 0) {
			g = 256 + g;
		}
		int b = pixels[(x + y * width) * mul + 2];
		if (b < 0) {
			b = 256 + b;
		}
		if (!tex.hasAlpha()) {
			return new Color(r, g, b);
		}
		int a = pixels[(x + y * width) * mul + 3];
		if (a < 0) {
			a = 256 + a;
		}
		return new Color(r, g, b, a);
	}

}
