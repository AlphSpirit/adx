package render;

import java.util.ArrayList;
import java.util.List;

import math.ADXTransform;

import org.newdawn.slick.Color;

import render.ADXFont.CharDescriptor;

public class ADXFontFormatter {

	private static final int CODE_FONT = 1;
	private static final int CODE_COLOR = 2;

	private List<ADXFont> lFonts = new ArrayList<ADXFont>();

	private int hAlign = ADXFont.H_ALIGN_LEFT;
	private int vAlign = ADXFont.V_ALIGN_TOP;
	private int inVAlign = ADXFont.V_ALIGN_BOTTOM;

	public ADXFontFormatter(ADXFont... fonts) {
		for (ADXFont f : fonts) {
			lFonts.add(f);
		}
	}

	public void setAlign(int hAlign, int vAlign) {
		this.hAlign = hAlign;
		this.vAlign = vAlign;
	}

	public void setInAlign(int inVAlign) {
		this.inVAlign = inVAlign;
	}

	public void draw(ADXGraphics g, String str, int x, int y, Color color) {

		int c;
		CharDescriptor d;
		ADXFont currentFont = lFonts.get(0);
		int xDraw = x;
		int yDraw = y;
		Color colorDraw = color;
		float angleDraw = 0;
		boolean angle = false;
		int[] lineHeight = getLineHeight(str);
		int[] lineWidth = getLineWidth(str);
		int[] lineBase = getLineBase(str);
		int totalHeight = 0;
		for (int i = 0; i < lineHeight.length; i++) {
			totalHeight += lineHeight[i];
		}
		String strDraw;
		String[] split = str.split("\\n");
		int xPlus;
		int yPlus;

		if (vAlign == ADXFont.V_ALIGN_BOTTOM) {
			yDraw -= totalHeight;
		} else if (vAlign == ADXFont.V_ALIGN_MIDDLE) {
			yDraw -= totalHeight / 2;
		}

		for (int i = 0; i < split.length; i++) {
			strDraw = split[i];
			if (hAlign == ADXFont.H_ALIGN_RIGHT) {
				xDraw -= lineWidth[i];
			} else if (hAlign == ADXFont.H_ALIGN_CENTER) {
				xDraw -= lineWidth[i] / 2;
			}
			for (int j = 0; j < strDraw.length(); j++) {
				c = (int) strDraw.charAt(j);
				if (c == CODE_FONT) {
					j++;
					c = (int) strDraw.charAt(j);
					currentFont = lFonts.get(c);
				} else if (c == CODE_COLOR) {
					j++;
					int red = (int) strDraw.charAt(j);
					int green = (int) strDraw.charAt(j + 1);
					int blue = (int) strDraw.charAt(j + 2);
					j += 2;
					colorDraw = new Color(red, green, blue);
				} else {
					d = currentFont.getCharacterMap().get(c);
					if (d != null) {
						xPlus = 0;
						yPlus = 0;
						if (inVAlign == ADXFont.V_ALIGN_BOTTOM) {
							yPlus += lineBase[i] - currentFont.getBase();
						} else if (inVAlign == ADXFont.V_ALIGN_MIDDLE) {
							yPlus += (lineBase[i] - currentFont.getBase()) / 2;
						}
						int xFinal = xDraw + d.xOffset + xPlus;
						int yFinal = yDraw + d.yOffset + yPlus;
						if (angle) {
							g.pushTransformation(ADXTransform.T_ROTATE_FROM, xFinal + d.width / 2, yFinal + d.height / 2, angleDraw);
						}
						g.drawTexture(d.textureID, xFinal, yFinal, d.width, d.height, d.tx1, d.ty1, d.tx2, d.ty2, colorDraw);
						if (angle) {
							g.popTransformation();
						}
						xDraw += d.xAdvance;
					}
				}
			}
			xDraw = x;
			yDraw += lineHeight[i];
		}

	}

	public int[] getLineHeight(String str) {
		int c;
		ADXFont currentFont = lFonts.get(0);
		String[] split = str.split("\\n");
		String strDraw;
		int[] height = new int[split.length];
		for (int i = 0; i < height.length; i++) {
			strDraw = split[i];
			int maxHeight = currentFont.getLineHeight();
			for (int j = 0; j < strDraw.length(); j++) {
				c = (int) strDraw.charAt(j);
				if (c == 1) {
					j++;
					c = (int) strDraw.charAt(j);
					currentFont = lFonts.get(c);
					if (currentFont.getLineHeight() > maxHeight) {
						maxHeight = currentFont.getLineHeight();
					}
				}
			}
			height[i] = maxHeight;
		}
		return height;
	}
	
	public int getTotalHeight(String str) {
		int[] lines = getLineHeight(str);
		int total = 0;
		for (int i : lines) {
			total += i;
		}
		return total;
	}

	public int[] getLineBase(String str) {
		int c;
		ADXFont currentFont = lFonts.get(0);
		String[] split = str.split("\\n");
		String strDraw;
		int[] base = new int[split.length];
		for (int i = 0; i < base.length; i++) {
			strDraw = split[i];
			int maxBase = currentFont.getBase();
			for (int j = 0; j < strDraw.length(); j++) {
				c = (int) strDraw.charAt(j);
				if (c == 1) {
					j++;
					c = (int) strDraw.charAt(j);
					currentFont = lFonts.get(c);
					if (currentFont.getBase() > maxBase) {
						maxBase = currentFont.getBase();
					}
				}
			}
			base[i] = maxBase;
		}
		return base;
	}

	public int[] getLineWidth(String str) {
		int c;
		CharDescriptor d;
		ADXFont currentFont = lFonts.get(0);
		String[] split = str.split("\\n");
		String strDraw;
		int[] width = new int[split.length];
		for (int i = 0; i < width.length; i++) {
			strDraw = split[i];
			int widthTotal = 0;
			for (int j = 0; j < strDraw.length(); j++) {
				c = (int) strDraw.charAt(j);
				if (c == CODE_FONT) {
					j++;
					c = (int) strDraw.charAt(j);
					currentFont = lFonts.get(c);
				} else if (c == CODE_COLOR) {
					j += 3;
				} else {
					d = currentFont.getCharacterMap().get(c);
					if (d != null) {
						widthTotal += d.xAdvance;
					}
				}
			}
			width[i] = widthTotal;
		}
		return width;
	}
	
	public int getMaxWidth(String str) {
		int[] lines = getLineWidth(str);
		int max = 0;
		for (int i : lines) {
			if (i > max) {
				max = i;
			}
		}
		return max;
	}

	public String changeFont(int font) {
		return "" + (char) (CODE_FONT) + (char) (font);
	}

	public String changeColor(int red, int green, int blue) {
		return "" + (char) (CODE_COLOR) + (char) (red) + (char) (green) + (char) (blue);
	}

	public String changeColor(Color c) {
		return changeColor(c.getRed(), c.getGreen(), c.getBlue());
	}

}
