package render;

import static org.lwjgl.opengl.EXTFramebufferObject.GL_FRAMEBUFFER_EXT;
import static org.lwjgl.opengl.EXTFramebufferObject.glBindFramebufferEXT;
import math.ADXTransform;

import org.lwjgl.opengl.ARBShaderObjects;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.Util;
import org.newdawn.slick.Color;

import frame.ADXGame;
import frame.ADXView;

public class ADXGraphics {

	private static final byte CODE_QUAD = 1;
	private static final byte CODE_QUAD_COL = 2;
	private static final byte CODE_QUAD_TEX = 3;
	private static final byte CODE_QUAD_COL_TEX = 4;
	private static final byte CODE_LINE = 5;
	private static final byte CODE_POLYGON = 6;
	private static final byte CODE_POLYGON_TEX = 7;
	private static final byte CODE_COLOR = 8;
	private static final byte CODE_SHADER = 9;
	private static final byte CODE_BLEND = 10;
	private static final byte CODE_BIND = 11;

	private static final byte SHAPE_NONE = 0;
	private static final byte SHAPE_QUAD = 1;
	private static final byte SHAPE_LINE = 2;

	private int bufferSizeStart;
	private int bufferSizeInc;

	private int pos = 0;
	private int bufferSize;
	private float[] buffer;

	private byte[] aTransform = new byte[10];
	private float[][] aTransformArgs = new float[10][];
	private int transformSize = 0;
	private int circlePrecision = 32;
	
	private ADXView currentView;
	private int currentViewID;
	private int viewX;
	private int viewY;
	private int viewWidth;
	private int viewHeight;
	
	private boolean debug = false;

	public ADXGraphics(int bSizeStart, int bSizeInc) {
		bufferSizeStart = bSizeStart;
		bufferSizeInc = bSizeInc;
		bufferSize = bufferSizeStart;
		buffer = new float[bufferSizeStart];
	}
	
	public ADXView getView() {
		return currentView;
	}
	
	public int getViewID() {
		return currentViewID;
	}
	
	public int getViewX() {
		return viewX;
	}
	
	public int getViewY() {
		return viewY;
	}
	
	public int getViewWidth() {
		return viewWidth;
	}
	
	public int getViewHeight() {
		return viewHeight;
	}
	
	public int getCirclePrecision() {
		return circlePrecision;
	}
	
	public void setCirclePrecision(int precision) {
		circlePrecision = precision;
	}
	
	public void setDebug(boolean d) {
		debug = d;
	}

	public void startDraw(ADXGame game, ADXView view, int id) {
		pos = 0;
		currentView = view;
		currentViewID = id;
		if (view != null) {
			viewX = view.getX();
			viewY = view.getY();
			viewWidth = view.getWidth();
			viewHeight = view.getHeight();
		} else {
			viewX = 0;
			viewY = 0;
			viewWidth = game.getWidth();
			viewHeight = game.getHeight();
		}
	}

	public void flushDraw() {
		int draw = 0;
		int code;
		int currentShape = 0;
		int currentTexture = -1;
		while (draw < pos) {
			code = (int) buffer[draw++];
			switch (code) {
			case CODE_QUAD:
				if (currentShape != SHAPE_QUAD || currentTexture != 0) {
					if (currentShape != SHAPE_NONE || (currentShape == SHAPE_QUAD && currentTexture != 0 && currentTexture != -1)) {
						glEnd();
						postGLDebug("glEnd()");
					}
					if (currentTexture != 0) {
						glBindTexture(GL_TEXTURE_2D, 0);
						currentTexture = 0;
					}
					glBegin(GL_QUADS);
					postGLDebug("glBegin(QUAD)");
					currentShape = SHAPE_QUAD;
				}
				glVertex2f(buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				break;
			case CODE_QUAD_COL:
				if (currentShape != SHAPE_QUAD || currentTexture != 0) {
					if (currentShape != SHAPE_NONE || (currentShape == SHAPE_QUAD && currentTexture != 0 && currentTexture != -1)) {
						glEnd();
						postGLDebug("glEnd()");
					}
					if (currentTexture != 0) {
						glBindTexture(GL_TEXTURE_2D, 0);
						currentTexture = 0;
					}
					glBegin(GL_QUADS);
					postGLDebug("glBegin(QUAD_COL)");
					currentShape = SHAPE_QUAD;
				}
				glColor4f(buffer[draw++], buffer[draw++], buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				glColor4f(buffer[draw++], buffer[draw++], buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				glColor4f(buffer[draw++], buffer[draw++], buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				glColor4f(buffer[draw++], buffer[draw++], buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				break;
			case CODE_QUAD_TEX:
				int tex = (int) buffer[draw++];
				if (currentShape != SHAPE_QUAD || currentTexture != tex) {
					if (currentShape != SHAPE_NONE || (currentShape == SHAPE_QUAD && currentTexture != tex && currentTexture != -1)) {
						glEnd();
						postGLDebug("glEnd()");
					}
					if (currentTexture != tex) {
						glBindTexture(GL_TEXTURE_2D, tex);
						currentTexture = tex;
					}
					glBegin(GL_QUADS);
					postGLDebug("glBegin(QUAD_TEX)");
					currentShape = SHAPE_QUAD;
				}
				glTexCoord2f(buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				glTexCoord2f(buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				glTexCoord2f(buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				glTexCoord2f(buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				break;
			case CODE_QUAD_COL_TEX:
				tex = (int) buffer[draw++];
				if (currentShape != SHAPE_QUAD || currentTexture != tex) {
					if (currentShape != SHAPE_NONE || (currentShape == SHAPE_QUAD && currentTexture != tex && currentTexture != -1)) {
						glEnd();
						postGLDebug("glEnd()");
					}
					if (currentTexture != tex) {
						glBindTexture(GL_TEXTURE_2D, tex);
						currentTexture = tex;
					}
					glBegin(GL_QUADS);
					postGLDebug("glBegin(QUAD_COL_TEX)");
					currentShape = SHAPE_QUAD;
				}
				glColor4f(buffer[draw++], buffer[draw++], buffer[draw++], buffer[draw++]);
				glTexCoord2f(buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				glColor4f(buffer[draw++], buffer[draw++], buffer[draw++], buffer[draw++]);
				glTexCoord2f(buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				glColor4f(buffer[draw++], buffer[draw++], buffer[draw++], buffer[draw++]);
				glTexCoord2f(buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				glColor4f(buffer[draw++], buffer[draw++], buffer[draw++], buffer[draw++]);
				glTexCoord2f(buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				break;
			case CODE_LINE:
				if (currentShape != SHAPE_LINE || currentTexture != 0) {
					if (currentShape != SHAPE_NONE || (currentShape == SHAPE_LINE && currentTexture != 0 && currentTexture != -1)) {
						glEnd();
						postGLDebug("glEnd()");
					}
					if (currentTexture != 0) {
						glBindTexture(GL_TEXTURE_2D, 0);
						currentTexture = 0;
					}
					glBegin(GL_LINES);
					postGLDebug("glBegin(LINE)");
					currentShape = SHAPE_LINE;
				}
				glVertex2f(buffer[draw++], buffer[draw++]);
				glVertex2f(buffer[draw++], buffer[draw++]);
				break;
			case CODE_POLYGON:
				if (currentShape != SHAPE_NONE) {
					glEnd();
					postGLDebug("glEnd()");
					currentShape = SHAPE_NONE;
				}
				if (currentTexture != 0) {
					glBindTexture(GL_TEXTURE_2D, 0);
					currentTexture = 0;
				}
				glBegin((int) buffer[draw++]);
				postGLDebug("glBegin(POLYGON)");
				int length = (int) buffer[draw++];
				for (int i = 0; i < length; i++) {
					glVertex2f(buffer[draw++], buffer[draw++]);
				}
				glEnd();
				postGLDebug("glEnd()");
				break;
			case CODE_POLYGON_TEX:
				tex = (int) buffer[draw++];
				if (currentShape != SHAPE_NONE) {
					glEnd();
					currentShape = SHAPE_NONE;
				}
				if (currentTexture != tex) {
					glBindTexture(GL_TEXTURE_2D, tex);
					currentTexture = tex;
				}
				glBegin((int) buffer[draw++]);
				length = (int) buffer[draw++];
				for (int i = 0; i < length; i++) {
					glTexCoord2f(buffer[draw++], buffer[draw++]);
					glVertex2f(buffer[draw++], buffer[draw++]);
				}
				glEnd();
				break;
			case CODE_COLOR:
				glColor4f(buffer[draw++], buffer[draw++], buffer[draw++], buffer[draw++]);
				break;
			case CODE_SHADER:
				if (currentShape != SHAPE_NONE) {
					glEnd();
					currentShape = SHAPE_NONE;
				}
				ARBShaderObjects.glUseProgramObjectARB((int) buffer[draw++]);
				break;
			case CODE_BLEND:
				glBlendFunc((int) buffer[draw++], (int) buffer[draw++]);
				break;
			case CODE_BIND:
				if (currentShape != SHAPE_NONE) {
					glEnd();
					currentShape = SHAPE_NONE;
				}
				glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, (int) buffer[draw++]);
				break;
			}
		}
		if (currentShape != SHAPE_NONE) {
			glEnd();
			postGLDebug("glEnd()");
		}
		Util.checkGLError();
	}

	public void pushTransformation(byte type, float... args) {
		aTransform[transformSize] = type;
		aTransformArgs[transformSize] = args;
		transformSize++;
	}

	public void popTransformation() {
		if (transformSize > 0) {
			transformSize--;
		}
	}

	public void clearTransformations() {
		transformSize = 0;
	}
	
	/* Line */

	public void drawLine(float x1, float y1, float x2, float y2) {
		insertLine(x1, y1, x2, y2);
	}

	public void drawLine(float x1, float y1, float x2, float y2, Color c) {
		insertColor(c);
		insertLine(x1, y1, x2, y2);
	}
	
	/* Rectangle */

	public void drawRectangle(float x, float y, float width, float height) {
		insertQuad(x, y, x + width, y, x + width, y + height, x, y + height);
	}

	public void drawRectangle(float x, float y, float width, float height, Color c) {
		insertColor(c);
		insertQuad(x, y, x + width, y, x + width, y + height, x, y + height);
	}

	public void drawRectangle(float x, float y, float width, float height, Color c1, Color c2, Color c3, Color c4) {
		insertColoredQuad(x, y, x + width, y, x + width, y + height, x, y + height, c1, c2, c3, c4);
	}
	
	/* Sprite */
	
	public void drawSprite(ADXSprite spr, float x, float y) {
		float x1 = x - spr.getOriginX();
		float y1 = y - spr.getOriginY();
		float x2 = x - spr.getOriginX() + spr.getWidth();
		float y2 = y - spr.getOriginY() + spr.getHeight();
		insertTexturedQuad(x1, y1, x2, y1, x2, y2, x1, y2, spr.getTextureID(),
				0, 0, spr.getImageWidth(), 0, spr.getImageWidth(), spr.getImageHeight(), 0, spr.getImageHeight());
	}
	
	public void drawSprite(ADXSprite spr, float x, float y, Color c) {
		float x1 = x - spr.getOriginX();
		float y1 = y - spr.getOriginY();
		float x2 = x - spr.getOriginX() + spr.getWidth();
		float y2 = y - spr.getOriginY() + spr.getHeight();
		insertColor(c);
		insertTexturedQuad(x1, y1, x2, y1, x2, y2, x1, y2, spr.getTextureID(),
				0, 0, spr.getImageWidth(), 0, spr.getImageWidth(), spr.getImageHeight(), 0, spr.getImageHeight());
	}
	
	public void drawSprite(ADXSprite spr, float x, float y, Color c1, Color c2, Color c3, Color c4) {
		float x1 = x - spr.getOriginX();
		float y1 = y - spr.getOriginY();
		float x2 = x - spr.getOriginX() + spr.getWidth();
		float y2 = y - spr.getOriginY() + spr.getHeight();
		insertColoredTexturedQuad(x1, y1, x2, y1, x2, y2, x1, y2, spr.getTextureID(),
				0, 0, spr.getImageWidth(), 0, spr.getImageWidth(), spr.getImageHeight(), 0, spr.getImageHeight(), c1, c2, c3, c4);
	}
	
	public void drawSprite(ADXSprite spr, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
		x1 -= spr.getOriginX();
		y1 -= spr.getOriginY();
		x2 -= spr.getOriginX();
		y2 -= spr.getOriginY();
		x3 -= spr.getOriginX();
		y3 -= spr.getOriginY();
		x4 -= spr.getOriginX();
		y4 -= spr.getOriginY();
		insertTexturedQuad(x1, y1, x2, y2, x3, y3, x4, y4, spr.getTextureID(),
				0, 0, spr.getImageWidth(), 0, spr.getImageWidth(), spr.getImageHeight(), 0, spr.getImageHeight());
	}
	
	public void drawSprite(ADXSprite spr, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, Color c) {
		x1 -= spr.getOriginX();
		y1 -= spr.getOriginY();
		x2 -= spr.getOriginX();
		y2 -= spr.getOriginY();
		x3 -= spr.getOriginX();
		y3 -= spr.getOriginY();
		x4 -= spr.getOriginX();
		y4 -= spr.getOriginY();
		insertColor(c);
		insertTexturedQuad(x1, y1, x2, y2, x3, y3, x4, y4, spr.getTextureID(),
				0, 0, spr.getImageWidth(), 0, spr.getImageWidth(), spr.getImageHeight(), 0, spr.getImageHeight());
	}
	
	public void drawSprite(ADXSprite spr, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4,
			Color c1, Color c2, Color c3, Color c4) {
		x1 -= spr.getOriginX();
		y1 -= spr.getOriginY();
		x2 -= spr.getOriginX();
		y2 -= spr.getOriginY();
		x3 -= spr.getOriginX();
		y3 -= spr.getOriginY();
		x4 -= spr.getOriginX();
		y4 -= spr.getOriginY();
		insertColoredTexturedQuad(x1, y1, x2, y2, x3, y3, x4, y4, spr.getTextureID(),
				0, 0, spr.getImageWidth(), 0, spr.getImageWidth(), spr.getImageHeight(), 0, spr.getImageHeight(), c1, c2, c3, c4);
	}
	
	/* Animated Sprite */
	
	public void drawAnimSprite(ADXSprite spr, int imageX, int imageY, float x, float y) {
		float x1 = x - spr.getOriginX();
		float y1 = y - spr.getOriginY();
		float x2 = x - spr.getOriginX() + spr.getWidth();
		float y2 = y - spr.getOriginY() + spr.getHeight();
		float tx1 = imageX * spr.getImageWidth();
		float ty1 = imageY * spr.getImageHeight();
		float tx2 = (imageX + 1) * spr.getImageWidth();
		float ty2 = (imageY + 1) * spr.getImageHeight();
		insertTexturedQuad(x1, y1, x2, y1, x2, y2, x1, y2, spr.getTextureID(), tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2);
	}
	
	public void drawAnimSprite(ADXSprite spr, int imageX, int imageY, float x, float y, Color c) {
		float x1 = x - spr.getOriginX();
		float y1 = y - spr.getOriginY();
		float x2 = x - spr.getOriginX() + spr.getWidth();
		float y2 = y - spr.getOriginY() + spr.getHeight();
		float tx1 = imageX * spr.getImageWidth();
		float ty1 = imageY * spr.getImageHeight();
		float tx2 = (imageX + 1) * spr.getImageWidth();
		float ty2 = (imageY + 1) * spr.getImageHeight();
		insertColor(c);
		insertTexturedQuad(x1, y1, x2, y1, x2, y2, x1, y2, spr.getTextureID(), tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2);
	}
	
	/* Partial Sprite */
	
	public void drawPartialSprite(ADXSprite spr, int imageX, int imageY, int countX, int countY, float x, float y) {
		float x1 = x - spr.getOriginX();
		float y1 = y - spr.getOriginY();
		float x2 = x - spr.getOriginX() + spr.getWidth();
		float y2 = y - spr.getOriginY() + spr.getHeight();
		float tx1 = imageX * spr.getImageWidth();
		float ty1 = imageY * spr.getImageHeight();
		float tx2 = (imageX + countX) * spr.getImageWidth();
		float ty2 = (imageY + countY) * spr.getImageHeight();
		insertTexturedQuad(x1, y1, x2, y1, x2, y2, x1, y2, spr.getTextureID(), tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2);
	}
	
	public void drawPartialSprite(ADXSprite spr, int imageX, int imageY, int countX, int countY, float x, float y, Color c) {
		float x1 = x - spr.getOriginX();
		float y1 = y - spr.getOriginY();
		float x2 = x - spr.getOriginX() + spr.getWidth();
		float y2 = y - spr.getOriginY() + spr.getHeight();
		float tx1 = imageX * spr.getImageWidth();
		float ty1 = imageY * spr.getImageHeight();
		float tx2 = (imageX + countX) * spr.getImageWidth();
		float ty2 = (imageY + countY) * spr.getImageHeight();
		insertColor(c);
		insertTexturedQuad(x1, y1, x2, y1, x2, y2, x1, y2, spr.getTextureID(), tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2);
	}
	
	/* Texture */
	
	public void drawTexture(int textureID, float x, float y, float width, float height) {
		insertTexturedQuad(x, y, x + width, y, x + width, y + height, x, y + height, textureID, 0, 0, 1, 0, 1, 1, 0, 1);
	}
	
	public void drawTexture(int textureID, float x, float y, float width, float height, Color c) {
		insertColor(c);
		insertTexturedQuad(x, y, x + width, y, x + width, y + height, x, y + height, textureID, 0, 0, 1, 0, 1, 1, 0, 1);
	}

	public void drawTexture(int textureID, float x, float y, float width, float height, float tx1, float ty1, float tx2, float ty2) {
		insertTexturedQuad(x, y, x + width, y, x + width, y + height, x, y + height, textureID, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2);
	}

	public void drawTexture(int textureID, float x, float y, float width, float height, float tx1, float ty1, float tx2, float ty2, Color c) {
		insertColor(c);
		insertTexturedQuad(x, y, x + width, y, x + width, y + height, x, y + height, textureID, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2);
	}
	
	/* Circle */
	
	public void drawCircle(float x, float y, float radius) {
		float[] px = new float[circlePrecision + 2];
		float[] py = new float[circlePrecision + 2];
		px[0] = x;
		py[0] = y;
		for (int i = 0; i < circlePrecision + 1; i++) {
			px[i + 1] = x + radius * (float) Math.cos(i * Math.PI * 2 / circlePrecision);
			py[i + 1] = y + radius * (float) Math.sin(i * Math.PI * 2 / circlePrecision);
		}
		drawPolygon(GL_TRIANGLE_FAN, px, py);
	}
	
	public void drawCircle(float x, float y, float radius, Color c) {
		insertColor(c);
		float[] px = new float[circlePrecision + 2];
		float[] py = new float[circlePrecision + 2];
		px[0] = x;
		py[0] = y;
		for (int i = 0; i < circlePrecision + 1; i++) {
			px[i + 1] = x + radius * (float) Math.cos(i * Math.PI * 2 / circlePrecision);
			py[i + 1] = y + radius * (float) Math.sin(i * Math.PI * 2 / circlePrecision);
		}
		drawPolygon(GL_TRIANGLE_FAN, px, py);
	}
	
	/* Polygon */
	
	public void drawPolygon(int type, float[] x, float[] y) {
		insertPolygon(type, x, y);
	}
	
	public void drawPolygon(int type, float[] x, float[] y, Color c) {
		insertColor(c);
		insertPolygon(type, x, y);
	}
	
	public void drawPolygon(int type, int textureID, float[] x, float[] y, float[] tx, float[] ty) {
		insertTexturedPolygon(textureID, type, x, y, tx, ty);
	}
	
	public void drawPolygon(int type, int textureID, float[] x, float[] y, float[] tx, float[] ty, Color c) {
		insertColor(c);
		insertTexturedPolygon(textureID, type, x, y, tx, ty);
	}

	/* Text */
	
	public void drawText(ADXFont fnt, String text, float x, float y) {
		fnt.draw(this, text, (int) x, (int) y, Color.white);
	}

	public void drawText(ADXFont fnt, String text, float x, float y, Color c) {
		fnt.draw(this, text, (int) x, (int) y, c);
	}
	
	/* Use Shader */
	
	public void useShader(ADXShader shader) {
		int program;
		if (shader != null) {
			program = shader.getProgram();
		} else {
			program = 0;
		}
		checkBuffer(2);
		buffer[pos++] = CODE_SHADER;
		buffer[pos++] = program;
	}
	
	/* Blend function */
	
	public void blendFunction(int src, int dst) {
		checkBuffer(3);
		buffer[pos++] = CODE_BLEND;
		buffer[pos++] = src;
		buffer[pos++] = dst;
	}
	
	/* Binding function */
	
	public void bindTexture(ADXTexture tex) {
		int id;
		if (tex != null) {
			id = tex.getTextureID();
		} else {
			id = 0;
		}
		checkBuffer(2);
		buffer[pos++] = CODE_BIND;
		buffer[pos++] = id;
	}
	
	/* Buffer functions */
	
	private void insertLine(float x1, float y1, float x2, float y2) {
		checkBuffer(5);
		buffer[pos++] = CODE_LINE;
		if (transformSize == 0) {
			insertRawPosition(x1 - viewX, y1 - viewY);
			insertRawPosition(x2 - viewX, y2 - viewY);
		} else {
			ADXTransform m = new ADXTransform(x1, y1, x2, y2);
			m.applyTransformations(aTransform, aTransformArgs, transformSize);
			insertRawPosition(m.getX(0) - viewX, m.getY(0) - viewY);
			insertRawPosition(m.getX(1) - viewX, m.getY(1) - viewY);
		}
	}

	private void insertQuad(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
		checkBuffer(9);
		buffer[pos++] = CODE_QUAD;
		if (transformSize == 0) {
			insertRawPosition(x1 - viewX, y1 - viewY);
			insertRawPosition(x2 - viewX, y2 - viewY);
			insertRawPosition(x3 - viewX, y3 - viewY);
			insertRawPosition(x4 - viewX, y4 - viewY);
		} else {
			ADXTransform m = new ADXTransform(x1, y1, x2, y2, x3, y3, x4, y4);
			m.applyTransformations(aTransform, aTransformArgs, transformSize);
			insertRawPosition(m.getX(0) - viewX, m.getY(0) - viewY);
			insertRawPosition(m.getX(1) - viewX, m.getY(1) - viewY);
			insertRawPosition(m.getX(2) - viewX, m.getY(2) - viewY);
			insertRawPosition(m.getX(3) - viewX, m.getY(3) - viewY);
		}
	}

	private void insertColoredQuad(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, Color c1, Color c2, Color c3, Color c4) {
		checkBuffer(0);
		buffer[pos++] = CODE_QUAD_COL;
		if (transformSize == 0) {
			insertRawColor(c1);
			insertRawPosition(x1 - viewX, y1 - viewY);
			insertRawColor(c2);
			insertRawPosition(x2 - viewX, y2 - viewY);
			insertRawColor(c3);
			insertRawPosition(x3 - viewX, y3 - viewY);
			insertRawColor(c4);
			insertRawPosition(x4 - viewX, y4 - viewY);
		} else {
			ADXTransform m = new ADXTransform(x1, y1, x2, y2, x3, y3, x4, y4);
			m.applyTransformations(aTransform, aTransformArgs, transformSize);
			insertRawColor(c1);
			insertRawPosition(m.getX(0) - viewX, m.getY(0) - viewY);
			insertRawColor(c2);
			insertRawPosition(m.getX(1) - viewX, m.getY(1) - viewY);
			insertRawColor(c3);
			insertRawPosition(m.getX(2) - viewX, m.getY(2) - viewY);
			insertRawColor(c4);
			insertRawPosition(m.getX(3) - viewX, m.getY(3) - viewY);
		}
	}

	private void insertTexturedQuad(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4,
			float textureID, float tx1, float ty1, float tx2, float ty2, float tx3, float ty3, float tx4, float ty4) {
		checkBuffer(18);
		buffer[pos++] = CODE_QUAD_TEX;
		buffer[pos++] = textureID;
		if (transformSize == 0) {
			insertRawPosition(tx1, ty1);
			insertRawPosition(x1 - viewX, y1 - viewY);
			insertRawPosition(tx2, ty2);
			insertRawPosition(x2 - viewX, y2 - viewY);
			insertRawPosition(tx3, ty3);
			insertRawPosition(x3 - viewX, y3 - viewY);
			insertRawPosition(tx4, ty4);
			insertRawPosition(x4 - viewX, y4 - viewY);
		} else {
			ADXTransform m = new ADXTransform(x1, y1, x2, y2, x3, y3, x4, y4);
			m.applyTransformations(aTransform, aTransformArgs, transformSize);
			insertRawPosition(tx1, ty1);
			insertRawPosition(m.getX(0) - viewX, m.getY(0) - viewY);
			insertRawPosition(tx2, ty2);
			insertRawPosition(m.getX(1) - viewX, m.getY(1) - viewY);
			insertRawPosition(tx3, ty3);
			insertRawPosition(m.getX(2) - viewX, m.getY(2) - viewY);
			insertRawPosition(tx4, ty4);
			insertRawPosition(m.getX(3) - viewX, m.getY(3) - viewY);
		}
	}
	
	private void insertColoredTexturedQuad(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4,
			float textureID, float tx1, float ty1,float tx2, float ty2, float tx3, float ty3, float tx4, float ty4,
			Color c1, Color c2, Color c3, Color c4) {
		checkBuffer(0);
		buffer[pos++] = CODE_QUAD_COL;
		if (transformSize == 0) {
			insertRawColor(c1);
			insertRawPosition(tx1, ty1);
			insertRawPosition(x1 - viewX, y1 - viewY);
			insertRawColor(c2);
			insertRawPosition(tx2, ty2);
			insertRawPosition(x2 - viewX, y2 - viewY);
			insertRawColor(c3);
			insertRawPosition(tx3, ty3);
			insertRawPosition(x3 - viewX, y3 - viewY);
			insertRawColor(c4);
			insertRawPosition(tx4, ty4);
			insertRawPosition(x4 - viewX, y4 - viewY);
		} else {
			ADXTransform m = new ADXTransform(x1, y1, x2, y2, x3, y3, x4, y4);
			m.applyTransformations(aTransform, aTransformArgs, transformSize);
			insertRawColor(c1);
			insertRawPosition(tx1, ty1);
			insertRawPosition(m.getX(0) - viewX, m.getY(0) - viewY);
			insertRawColor(c2);
			insertRawPosition(tx2, ty2);
			insertRawPosition(m.getX(1) - viewX, m.getY(1) - viewY);
			insertRawColor(c3);
			insertRawPosition(tx3, ty3);
			insertRawPosition(m.getX(2) - viewX, m.getY(2) - viewY);
			insertRawColor(c4);
			insertRawPosition(tx4, ty4);
			insertRawPosition(m.getX(3) - viewX, m.getY(3) - viewY);
		}
	}

	private void insertColor(Color c) {
		checkBuffer(5);
		buffer[pos++] = CODE_COLOR;
		insertRawColor(c);
	}
	
	private void insertPolygon(int type, float[] x, float[] y) {
		if (x.length != y.length) {
			return;
		}
		int length = x.length;
		checkBuffer(3 + 2 * length);
		buffer[pos++] = CODE_POLYGON;
		buffer[pos++] = type;
		buffer[pos++] = length;
		if (transformSize == 0) {
			for (int i = 0; i < length; i++) {
				insertRawPosition(x[i] - viewX, y[i] - viewY);
			}
		} else {
			ADXTransform m = new ADXTransform(x, y);
			m.applyTransformations(aTransform, aTransformArgs, transformSize);
			for (int i = 0; i < length; i++) {
				insertRawPosition(m.getX(i) - viewX, m.getY(i) - viewY);
			}
		}
	}
	
	private void insertTexturedPolygon(int textureID, int type, float[] x, float[] y, float[] tx, float[] ty) {
		if (x.length != y.length || x.length != tx.length || x.length != ty.length) {
			return;
		}
		int length = x.length;
		checkBuffer(4 + 4 * length);
		buffer[pos++] = CODE_POLYGON_TEX;
		buffer[pos++] = textureID;
		buffer[pos++] = type;
		buffer[pos++] = length;
		if (transformSize == 0) {
			for (int i = 0; i < length; i++) {
				insertRawPosition(tx[i], ty[i]);
				insertRawPosition(x[i] - viewX, y[i] - viewY);
			}
		} else {
			ADXTransform m = new ADXTransform(x, y);
			m.applyTransformations(aTransform, aTransformArgs, transformSize);
			for (int i = 0; i < length; i++) {
				insertRawPosition(tx[i], ty[i]);
				insertRawPosition(m.getX(i) - viewX, m.getY(i) - viewY);
			}
		}
	}
	
	private void insertRawColor(Color c) {
		buffer[pos++] = c.r;
		buffer[pos++] = c.g;
		buffer[pos++] = c.b;
		buffer[pos++] = c.a;
	}
	
	private void insertRawPosition(float x, float y) {
		buffer[pos++] = x;
		buffer[pos++] = y;
	}

	private void checkBuffer(int add) {
		if (pos + add > bufferSize) {
			ADXGame.postWarning("Graphics buffer overflow, augmenting size to " + (bufferSize + bufferSizeInc));
			float[] temp = new float[bufferSize + bufferSizeInc];
			for (int i = 0; i < bufferSize; i++) {
				temp[i] = buffer[i];
			}
			buffer = temp;
			bufferSize = bufferSize + bufferSizeInc;
		}
	}
	
	private void postGLDebug(String str) {
		if (debug) {
			System.out.println(str);
		}
	}

}
