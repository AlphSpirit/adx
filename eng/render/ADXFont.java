package render;

import frame.ADXGame;
import io.ADXTextFile;

import java.util.HashMap;

import org.newdawn.slick.Color;

public class ADXFont {

	public static final int H_ALIGN_LEFT = 0;
	public static final int H_ALIGN_CENTER = 1;
	public static final int H_ALIGN_RIGHT = 2;
	public static final int V_ALIGN_TOP = 0;
	public static final int V_ALIGN_MIDDLE = 1;
	public static final int V_ALIGN_BOTTOM = 2;
	
	private static HashMap<String, ADXFont> mapFonts = new HashMap<String, ADXFont>();

	private ADXSprite[] aPages;
	private HashMap<Integer, CharDescriptor> mapChar = new HashMap<Integer, CharDescriptor>();

	private String name;
	private int lineHeight = 0;
	private int base = 0;
	private int pages = 0;

	private int hAlign = H_ALIGN_LEFT;
	private int vAlign = V_ALIGN_TOP;

	public class CharDescriptor {

		int character;
		int x;
		int y;
		int width;
		int height;
		int xOffset;
		int yOffset;
		int xAdvance;
		int page;
		int textureID;
		float tx1;
		float ty1;
		float tx2;
		float ty2;

		public CharDescriptor(int character, int x, int y, int width, int height, int xOffset, int yOffset, int xAdvance, int page, int textureID) {
			this.character = character;
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
			this.xOffset = xOffset;
			this.yOffset = yOffset;
			this.xAdvance = xAdvance;
			this.page = page;
			this.textureID = textureID;
			ADXSprite spr = aPages[page];
			tx1 = this.x / (float) spr.getWidth();
			ty1 = this.y / (float) spr.getHeight();
			tx2 = (x + width) / (float) spr.getWidth();
			ty2 = (y + height) / (float) spr.getHeight();
		}

	}

	public ADXFont(String path, String name) {
		
		this.name = name;
		
		if (!mapFonts.containsKey(path + "/" + name)) {
			
			ADXGame.postString("Loading font " + path + "/" + name + "...");
			ADXTextFile file = new ADXTextFile(path + "/" + name + ".fnt");
			if (!file.isExisting()) {
				ADXGame.postWarning("Font " + path + "/" + name + " was not found.");
				return;
			}
	
			String line;
			String[] args;
			while ((line = file.readLine()) != null) {
				// Splits on 1-or-more spaces and = characters
				args = line.split("\\s+|=");
				if (args[0].equals("page")) {
					int page = Integer.parseInt(args[2]);
					String pagePath = args[4].replace("\"", "");
					aPages[page] = new ADXSprite(path + "/" + pagePath);
				} else if (args[0].equals("common")) {
					lineHeight = Integer.parseInt(args[2]);
					base = Integer.parseInt(args[4]);
					pages = Integer.parseInt(args[10]);
					aPages = new ADXSprite[pages];
				} else if (args[0].equals("char")) {
					int character = Integer.parseInt(args[2]);
					int x = Integer.parseInt(args[4]);
					int y = Integer.parseInt(args[6]);
					int width = Integer.parseInt(args[8]);
					int height = Integer.parseInt(args[10]);
					int xOffset = Integer.parseInt(args[12]);
					int yOffset = Integer.parseInt(args[14]);
					int xAdvance = Integer.parseInt(args[16]);
					int page = Integer.parseInt(args[18]);
					mapChar.put(character, new CharDescriptor(character, x, y, width, height, xOffset, yOffset, xAdvance, page, aPages[page].getTextureID()));
				}
			}
			
			mapFonts.put(path + "/" + name, this);
			
		} else {
			
			ADXFont f = mapFonts.get(path + "/" + name);
			lineHeight = f.lineHeight;
			base = f.base;
			pages = f.pages;
			aPages = f.aPages;
			mapChar = f.mapChar;

		}
		
	}

	public String getName() {
		return name;
	}

	public int getLineHeight() {
		return lineHeight;
	}

	public int getBase() {
		return base;
	}

	public HashMap<Integer, CharDescriptor> getCharacterMap() {
		return mapChar;
	}

	public void setAlign(int hAlign, int vAlign) {
		this.hAlign = hAlign;
		this.vAlign = vAlign;
	}

	public void draw(ADXGraphics g, String str, int x, int y, Color color) {

		int c;
		CharDescriptor d;
		int xDraw = x;
		int yDraw = y;
		int lineWidth;

		String strDraw;
		String[] split = str.split("\\n");

		if (vAlign == V_ALIGN_BOTTOM) {
			yDraw -= split.length * lineHeight;
		} else if (vAlign == V_ALIGN_MIDDLE) {
			yDraw -= (split.length * lineHeight) / 2;
		}

		for (int i = 0; i < split.length; i++) {

			strDraw = split[i];
			if (hAlign != H_ALIGN_LEFT) {
				lineWidth = getWidth(strDraw);
				if (hAlign == H_ALIGN_RIGHT) {
					xDraw -= lineWidth;
				} else if (hAlign == H_ALIGN_CENTER) {
					xDraw -= lineWidth / 2;
				}
			}
			for (int j = 0; j < strDraw.length(); j++) {
				c = (int) strDraw.charAt(j);
				d = mapChar.get(c);
				if (d != null) {
					g.drawTexture(d.textureID, xDraw + d.xOffset, yDraw + d.yOffset, d.width, d.height, d.tx1, d.ty1, d.tx2, d.ty2, color);
					xDraw += d.xAdvance;
				}
			}
			xDraw = x;
			yDraw += lineHeight;

		}

	}

	public int getWidth(String str) {
		int c;
		CharDescriptor d;
		int width = 0;
		for (int i = 0; i < str.length(); i++) {
			c = (int) str.charAt(i);
			d = mapChar.get(c);
			if (d != null) {
				width += d.xAdvance;
			}
		}
		return width;
	}

}
