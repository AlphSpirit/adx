package gui;

import java.util.HashMap;

import io.ADXInput;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;

import frame.ADXGame;
import frame.ADXInstance;
import render.ADXFont;
import render.ADXGraphics;
import shapes.ADXPoint;
import shapes.ADXRectangle;

public class GUI extends ADXInstance {
	
	public static Color defaultColor = new Color(0.2f, 0.2f, 0.2f, 1.0f);
	public static ADXFont defaultFont = null;
	
	// Position and dimension
	protected int x;
	protected int y;
	protected int xPlus = 0;
	protected int yPlus = 0;
	protected int width;
	protected int height;
	protected ADXRectangle r;
	
	// States
	protected boolean hovered = false;
	protected boolean mouseDown = false;
	protected boolean clicked = false;
	protected boolean enabled = true;
	protected boolean visible = true;
	protected boolean selected = false;
	
	// States override
	protected boolean clickOver = false;
	
	// Linking
	protected HashMap<Integer, GUI> tab = new HashMap<Integer, GUI>();
	
	public GUI(int x, int y, int width, int height, int roomID) {
		this(x, y, width, height, roomID, 0);
	}
	
	public GUI(int x, int y, int width, int height, int roomID, int depth) {
		super(roomID, depth);
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		r = new ADXRectangle(x, y, width, height);
	}
	
	/* Accessors / Mutators */
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
		r.setX(x + xPlus);
		r.setY(y + yPlus);
	}
	
	public void setDecal(int x, int y) {
		this.xPlus = x;
		this.yPlus = y;
		r.setX(this.x + xPlus);
		r.setY(this.y + yPlus);
	}
	
	public boolean isClicked() {
		return clicked;
	}
	
	public void setClicked(boolean clicked) {
		this.clicked = clicked;
		clickOver = true;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean e) {
		enabled = e;
	}
	
	public boolean isSelected() {
		return selected;
	}
	
	public void setSelected(boolean s) {
		selected = s;
	}
	
	public void addTab(int key, GUI gui) {
		tab.put(key, gui);
	}
	
	public boolean isVisible() {
		return visible;
	}
	
	public void setVisible(boolean v) {
		visible = v;
	}
	
	@Override
	public void init(ADXGame game) {
		
	}
	
	@Override
	public void update(ADXGame game, ADXInput input) {
		
		if (!enabled) {
			hovered = false;
			mouseDown = false;
			clicked = false;
			selected = false;
			return;
		}
		
		ADXPoint p = new ADXPoint(input.getMouseX(), input.getMouseY());
		hovered = p.collides(r);
		mouseDown = hovered && input.getButton(ADXInput.MOUSE_LEFT);
		if (!clickOver) {
			clicked = hovered && input.getButtonReleased(ADXInput.MOUSE_LEFT) || (selected && input.getKeyPressed(Keyboard.KEY_RETURN));
		} else {
			clickOver = false;
		}
		
		if (clicked) {
			input.releaseButton(ADXInput.MOUSE_LEFT);
		}
		
		if (input.getButtonPressed(ADXInput.MOUSE_LEFT)) {
			selected = hovered;
		}
		
		if (selected && !tab.isEmpty()) {
			for (int i : tab.keySet()) {
				if (input.getKeyPressed(i)) {
					setSelected(false);
					tab.get(i).setSelected(true);
					input.releaseKey(i);
				}
			}
		}
		
	}
	
	@Override
	public void render(ADXGame game, ADXGraphics g) {
		
	}

}
