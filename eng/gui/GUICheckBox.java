package gui;

import io.ADXInput;

import org.newdawn.slick.Color;

import frame.ADXGame;
import render.ADXFont;
import render.ADXGraphics;

public class GUICheckBox extends GUI {
	
	private ADXFont fnt = GUI.defaultFont;
	private Color b = GUI.defaultColor;
	//private Color b = new Color(0.66f, 0.0f, 0.0f, 1.0f);
	private String text;
	private boolean checked;
	
	// Timer variables
	private int hoverTimer = 0;
	private int downTimer = 0;
	private int selectTimer = 0;

	public GUICheckBox(int x, int y, int width, int height, String text, int roomID) {
		super(x, y, width, height, roomID);
		this.text = text;
	}
	
	public boolean isChecked() {
		return checked;
	}
	
	public void setChecked(boolean c) {
		checked = c;
	}
	
	@Override
	public void init(ADXGame game) {
	}
	
	@Override
	public void update(ADXGame game, ADXInput input) {
		
		super.update(game, input);
		
		if (!enabled) {
			hoverTimer = 0;
			downTimer = 0;
			return;
		}
		
		if (hovered && hoverTimer < 10) {
			hoverTimer++;
		} else if (!hovered && hoverTimer > 0) {
			hoverTimer--;
		}
		
		if (mouseDown && downTimer < 5) {
			downTimer++;
		} else if (!mouseDown && downTimer > 0) {
			downTimer--;
		}
		
		if (clicked) {
			checked = !checked;
		}
		
		if (selected) {
			selectTimer++;
		} else {
			selectTimer = 0;
		}
		
	}
	
	@Override
	public void render(ADXGame game, ADXGraphics g) {
		
		if (!visible) {
			return;
		}
		
		float f = 0.1f + hoverTimer / 60.0f + downTimer / 30.0f;
		Color colFill1 = new Color(b.r + f, b.g + f, b.b + f, b.a);
		f -= 0.2f;
		Color colFill2 = new Color(b.r + f, b.g + f, b.b + f, b.a);
		Color colBorder;
		if (!enabled) {
			f = 0;
		} else if (selected) {
			f = (float)Math.sin(selectTimer / 15.0f) / 2.0f + 0.3f;
		} else {
			f = -0.2f;
		}
		colBorder = new Color(b.r + f, b.g + f, b.b + f, b.a);
		if (!enabled) {
			f = 0.2f;
		} else if (selected) {
			f = 0.6f;
		} else {
			f = 0.4f;
		}
		Color colInBorder = new Color(b.r + f, b.g + f, b.b + f, b.a);
		Color colText;
		if (enabled) {
			colText = Color.white;
		} else {
			colText = new Color(1.0f, 1.0f, 1.0f, 0.5f);
		}
		Color colCheck;
		if (enabled) {
			colCheck = Color.white;
		} else {
			colCheck = new Color(0.5f, 0.5f, 0.5f, 1.0f);
		}
		
		// Draw inside
		g.drawRectangle(x + 1, y + 1, width - 2, height - 2, colFill1, colFill1, colFill2, colFill2);
		
		// Draw inner border
		g.drawRectangle(x + 1, y + 1, width - 2, 1, colInBorder);
		g.drawRectangle(x + 1, y + 1, 1, height - 2, colInBorder);
		g.drawRectangle(x + 1, y + height - 2, width - 2, 1, colInBorder);
		g.drawRectangle(x + width - 2, y + 2, 1, height - 2, colInBorder);
		
		// Draw border
		g.drawRectangle(x, y, width, 1, colBorder);
		g.drawRectangle(x, y + height - 1, width, 1, colBorder);
		g.drawRectangle(x, y, 1, height, colBorder);
		g.drawRectangle(x + width - 1, y, 1, height, colBorder);
		
		if (checked) {
			g.drawRectangle(x + 8, y + 8, width - 16, 1, Color.white);
			g.drawRectangle(x + 8, y + height - 9, width - 16, 1, Color.white);
			g.drawRectangle(x + 8, y + 8, 1, height - 16, Color.white);
			g.drawRectangle(x + width - 9, y + 8, 1, height - 16, Color.white);
			g.drawRectangle(x + 8, y + 8, width - 16, height - 16, new Color(colCheck.r, colCheck.g, colCheck.b, colCheck.a - 0.5f));
		}
		
		if (fnt != null) {
			// Draw text
			fnt.setAlign(ADXFont.H_ALIGN_LEFT, ADXFont.V_ALIGN_MIDDLE);
			g.drawText(fnt, text, x + width  + 4, y + height / 2 + 1, Color.black);
			g.drawText(fnt, text, x + width + 4, y + height / 2, colText);
		}
		
	}

}
