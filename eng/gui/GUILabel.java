package gui;

import org.newdawn.slick.Color;

import frame.ADXGame;
import render.ADXFont;
import render.ADXGraphics;

public class GUILabel extends GUI {

	private ADXFont fnt = GUI.defaultFont;
	private String text;
	
	public GUILabel(int x, int y, int height, String text, int roomID) {
		super(x, y, 1, 32, roomID);
		this.text = text;
	}
	
	@Override
	public void init(ADXGame game) {
	}
	
	@Override
	public void render(ADXGame game, ADXGraphics g) {
		if (fnt != null) {
			fnt.setAlign(ADXFont.H_ALIGN_LEFT, ADXFont.V_ALIGN_MIDDLE);
			g.drawText(fnt, text, x + 4 + 1, y + height / 2 + 1, Color.black);
			g.drawText(fnt, text, x + 4, y + height / 2, Color.white);
		}
	}

}
