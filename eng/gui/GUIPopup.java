package gui;

import io.ADXInput;
import math.ADXMath;

import org.newdawn.slick.Color;

import frame.ADXGame;
import render.ADXFont;
import render.ADXFontFormatter;
import render.ADXGraphics;
import shapes.ADXRectangle;

public class GUIPopup extends GUI {
	
	private String text;
	private String button;
	private ADXFont fnt = GUI.defaultFont;
	private ADXFontFormatter fntFormat;
	private GUIButton btnOK;
	private boolean modal;
	private boolean closed = false;
	
	// Timer variables
	private int hoverTimer = 0;
	
	private Color b = GUI.defaultColor;
	
	public GUIPopup(String text, String button, boolean modal, int roomID) {
		this(text, button, modal, roomID, 0);
	}
	
	public GUIPopup(String text, String button, boolean modal, int roomID, int depth) {
		super(0, 0, 0, 0, roomID, depth);
		this.text = text;
		this.button = button;
		this.modal = modal;
	}
	
	public boolean isClosed() {
		return closed;
	}
	
	@Override
	public void init(ADXGame game) {
		
		if (fnt != null) {
			fntFormat = new ADXFontFormatter(fnt);
			width = (int) ADXMath.max(8 + fntFormat.getMaxWidth(text) + 8, 8 + 64 + 8);
			height = 8 + fntFormat.getTotalHeight(text) + 8 + 32 + 8;
		}
		x = (game.getWidth() - width) / 2;
		y = (game.getHeight() - height) / 2;
		r = new ADXRectangle(x, y, width, height);
		btnOK = new GUIButton(x + width - 64 - 8, y + height - 32 - 8, 64, 32, button, getRoomID(), getDepth());
		game.addInstance(btnOK);
		
	}
	
	@Override
	public void update(ADXGame game, ADXInput input) {
		
		super.update(game, input);
		
		if (btnOK.isClicked()) {
			closed = true;
			game.removeInstance(this);
			game.removeInstance(btnOK);
			return;
		}
		
		if (hovered && hoverTimer < 10) {
			hoverTimer++;
		} else if (!hovered && hoverTimer > 0) {
			hoverTimer--;
		}
		
		if (modal) {
			input.disableMousePosition();
		}
		
		x = (game.getWidth() - width) / 2;
		y = (game.getHeight() - height) / 2;
		btnOK.setPosition(x + width - 64 - 8, y + height - 32 - 8);
		
	}
	
	@Override
	public void render(ADXGame game, ADXGraphics g) {
		
		if (modal) {
			g.drawRectangle(0, 0, game.getWidth(), game.getHeight(), new Color(0.0f, 0.0f, 0.0f, 0.75f));
			g.drawRectangle(x, y, width, height, new Color(b.r, b.g, b.b, 0.66f + hoverTimer / 30.0f));
		} else {
			g.drawRectangle(x, y, width, height, new Color(b.r, b.g, b.b, 0.80f + hoverTimer / 50.0f));
		}
		
		float f = 0.3f;
		Color colInBorder = new Color(b.r + f, b.g + f, b.b + f, b.a);
		f = -0.3f;
		Color colOutBorder = new Color(b.r + f, b.g + f, b.b + f, b.a);
		
		// Draw inner border
		g.drawRectangle(x + 1, y + 1, width - 2, 1, colInBorder);
		g.drawRectangle(x + 1, y + 1, 1, height - 2, colInBorder);
		g.drawRectangle(x + 1, y + height - 2, width - 2, 1, colInBorder);
		g.drawRectangle(x + width - 2, y + 2, 1, height - 2, colInBorder);
		
		// Draw outer border
		g.drawRectangle(x, y, width, 1, colOutBorder);
		g.drawRectangle(x, y, 1, height,colOutBorder);
		g.drawRectangle(x, y + height -1 , width, 1, colOutBorder);
		g.drawRectangle(x + width - 1, y, 1, height, colOutBorder);
		
		if (fnt != null) {
			fntFormat.draw(g, text, x + 8 + 1, y + 8 + 1, Color.black);
			fntFormat.draw(g, text, x + 8, y + 8, Color.white);
		}
		
	}

}
