package gui;

import io.ADXInput;

import org.newdawn.slick.Color;

import render.ADXFont;
import render.ADXGraphics;
import frame.ADXGame;

public class GUIButton extends GUI {
	
	private ADXFont fnt = GUI.defaultFont;
	private Color b = GUI.defaultColor;
	//private Color b = new Color(0.66f, 0.0f, 0.0f, 1.0f);
	private String text;
	
	// Timer variables
	private int hoverTimer = 0;
	private int downTimer = 0;
	private int clickTimer = 0;
	private int selectTimer = 0;

	public GUIButton(int x, int y, int width, int height, String text, int roomID) {
		this(x, y, width, height, text, roomID, 0);
	}
	
	public GUIButton(int x, int y, int width, int height, String text, int roomID, int depth) {
		super(x, y, width, height, roomID, depth);
		this.text = text;
	}

	@Override
	public void init(ADXGame game) {
	}

	@Override
	public void update(ADXGame game, ADXInput input) {
		
		super.update(game, input);
		
		if (!enabled) {
			hoverTimer = 0;
			downTimer = 0;
			clickTimer = 0;
			return;
		}
		
		if (hovered && hoverTimer < 10) {
			hoverTimer++;
		} else if (!hovered && hoverTimer > 0) {
			hoverTimer--;
		}
		
		if (mouseDown && downTimer < 5) {
			downTimer++;
		} else if (!mouseDown && downTimer > 0) {
			downTimer--;
		}
		
		if (clicked) {
			clickTimer = 30;
		} else if (clickTimer > 0) {
			clickTimer--;
		}
		
		if (selected) {
			selectTimer++;
		} else {
			selectTimer = 0;
		}
		
	}

	@Override
	public void render(ADXGame game, ADXGraphics g) {
		
		if (!visible) {
			return;
		}
		
		float f = 0.2f + hoverTimer / 60.0f + downTimer / 30.0f + clickTimer / 60.0f;
		Color colFill1 = new Color(b.r + f, b.g + f, b.b + f, b.a);
		f -= 0.2f;
		Color colFill2 = new Color(b.r + f, b.g + f, b.b + f, b.a);
		Color colBorder;
		if (!enabled) {
			f = 0;
		} else if (selected) {
			f = (float)Math.sin(selectTimer / 15.0f) / 2.0f + 0.3f;
		} else {
			f = -0.2f;
		}
		colBorder = new Color(b.r + f, b.g + f, b.b + f, b.a);
		Color colText;
		if (enabled) {
			colText = Color.white;
		} else {
			colText = new Color(1.0f, 1.0f, 1.0f, 0.5f);
		}
		
		// Draw inside
		g.drawRectangle(x + 1, y + 1, width - 2, height - 2, colFill1, colFill1, colFill2, colFill2);
		
		// Draw border
		g.drawRectangle(x + 1, y, width - 2, 1, colBorder);
		g.drawRectangle(x + 1, y + height - 1, width - 2, 1, colBorder);
		g.drawRectangle(x, y + 1, 1, height - 2, colBorder);
		g.drawRectangle(x + width - 1, y + 1, 1, height - 2, colBorder);
		
		if (fnt != null) {
			// Draw text
			fnt.setAlign(ADXFont.H_ALIGN_CENTER, ADXFont.V_ALIGN_MIDDLE);
			int decal;
			if (!mouseDown) {
				decal = 1;
			} else {
				decal = 0;
			}
			g.drawText(fnt, text, x + width / 2 - decal + 1, y + height / 2 - decal + 1, Color.black);
			g.drawText(fnt, text, x + width / 2 - decal, y + height / 2 - decal, colText);
		}
		
	}

}
