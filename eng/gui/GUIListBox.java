package gui;

import io.ADXInput;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;

import frame.ADXGame;
import render.ADXGraphics;

public class GUIListBox extends GUI {
	
	public static final byte TYPE_LIST = 1;
	public static final byte TYPE_SELECT = 2;
	
	private int drawCount;
	private GUIListItem[] aItems;
	private String[] items;
	private boolean[] aActivated;
	private int decal = 0;
	private byte type;
	
	private Color b = GUI.defaultColor;
	
	private GUIButton btnUp;
	private GUIButton btnDown;

	public GUIListBox(int x, int y, int width, int height, int drawCount, byte type, int roomID, String... items) {
		super(x, y, width, height * drawCount, roomID);
		this.drawCount = drawCount;
		this.type = type;
		if (items != null) {
			this.items = items;
		} else {
			this.items = new String[0];
		}
	}
	
	public void setItems(String... items) {
		if (items != null) {
			this.items = items;
		} else {
			this.items = new String[0];
		}
		if (items.length <= drawCount) {
			btnUp.setEnabled(false);
			btnDown.setEnabled(false);
		}
		aActivated = new boolean[items.length];
		setTexts();
	}
	
	public void setActivated(boolean... activated) {
		if (activated.length == items.length) {
			aActivated = activated;
			setTexts();
		}
	}
	
	@Override
	public void init(ADXGame game) {
		aItems = new GUIListItem[drawCount];
		for (int i = 0; i < drawCount; i++) {
			aItems[i] = new GUIListItem(x, y + 32 * i, width - 32, height / drawCount, "", getRoomID());
			if (type == GUIListBox.TYPE_LIST) {
				aItems[i].setSelectable(false);
			}
			game.addInstance(aItems[i]);
		}
		btnUp = new GUIButton(x + width - 32, y, 32, height / 2, "^", getRoomID());
		game.addInstance(btnUp);
		btnDown = new GUIButton(x + width - 32, y + height / 2, 32, height / 2, "v", getRoomID());
		game.addInstance(btnDown);
		btnUp.addTab(Keyboard.KEY_TAB, btnDown);
		btnDown.addTab(Keyboard.KEY_TAB, btnUp);
		setItems(items);
	}
	
	@Override
	public void update(ADXGame game, ADXInput input) {
		
		for (int i = 0; i < drawCount; i++) {
			if (type == GUIListBox.TYPE_SELECT) {
				if (aItems[i].isVisible() && aItems[i].isClicked()) {
					aActivated[i + decal] = !aActivated[i + decal];
				}
			}
		}
		
		if (btnUp.isClicked() && decal > 0) {
			decal--;
			setTexts();
		}
		if (btnDown.isClicked() && decal < items.length - drawCount) {
			decal++;
			setTexts();
		}
		
	}
	
	@Override
	public void render(ADXGame game, ADXGraphics g) {
		
		g.drawRectangle(x, y, width, height, b);
		float f;
		Color colBorder;
		if (!enabled) {
			f = 0;
		} else {
			f = -0.1f;
		}
		colBorder = new Color(b.r + f, b.g + f, b.b + f, b.a);
		if (!enabled) {
			f = 0.1f;
		} else {
			f = 0.2f;
		}
		Color colInBorder = new Color(b.r + f, b.g + f, b.b + f, b.a);
		
		// Draw inner border
		g.drawRectangle(x + 1, y + 1, width - 2, 1, colInBorder);
		g.drawRectangle(x + 1, y + 1, 1, height - 2, colInBorder);
		g.drawRectangle(x + 1, y + height - 2, width - 2, 1, colInBorder);
		g.drawRectangle(x + width - 2, y + 2, 1, height - 2, colInBorder);
		
		// Draw border
		g.drawRectangle(x, y, width, 1, colBorder);
		g.drawRectangle(x, y + height - 1, width, 1, colBorder);
		g.drawRectangle(x, y, 1, height, colBorder);
		g.drawRectangle(x + width - 1, y, 1, height, colBorder);
		
	}
	
	private void setTexts() {
		for (int i = 0; i < drawCount; i++) {
			if (i + decal >= 0 && i + decal < items.length) {
				aItems[i].setText(items[i + decal]);
				aItems[i].setActivated(aActivated[i + decal]);
				aItems[i].setVisible(true);
			} else {
				aItems[i].setVisible(false);
			}
		}
	}

}
