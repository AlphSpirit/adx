package gui;

import io.ADXInput;

import org.newdawn.slick.Color;

import frame.ADXGame;
import render.ADXFont;
import render.ADXGraphics;

public class GUIListItem extends GUI {

	private ADXFont fnt = GUI.defaultFont;
	private Color b = GUI.defaultColor;
	private String text;
	private boolean selectable = true;
	
	// Timer variables
	private int hoverTimer = 0;
	private int downTimer = 0;
	
	private boolean activated = false;
	
	public GUIListItem(int x, int y, int width, int height, String text, int roomID) {
		super(x, y, width, height, roomID);
		this.text = text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public boolean getActivated() {
		return activated;
	}
	
	public void setActivated(boolean a) {
		activated = a;
	}
	
	public void setSelectable(boolean s) {
		selectable = s;
	}
	
	@Override
	public void init(ADXGame game) {
	}
	
	@Override
	public void update(ADXGame game, ADXInput input) {
		
		super.update(game, input);
		
		if (!enabled) {
			hoverTimer = 0;
			downTimer = 0;
			return;
		}
		
		if (hovered && hoverTimer < 10) {
			hoverTimer++;
		} else if (!hovered && hoverTimer > 0) {
			hoverTimer--;
		}
		
		if (mouseDown && downTimer < 5) {
			downTimer++;
		} else if (!mouseDown && downTimer > 0) {
			downTimer--;
		}
		
		if (clicked && selectable) {
			activated = !activated;
		}
		
	}
	
	@Override
	public void render(ADXGame game, ADXGraphics g) {
		
		if (!visible) {
			return;
		}
		
		float f = -0.1f + hoverTimer / 60.0f + downTimer / 40.0f;
		if (activated) {
			f += 0.4f;
		}
		Color colFill1 = new Color(b.r + f, b.g + f, b.b + f, b.a);
		f -= 0.1f;
		Color colFill2 = new Color(b.r + f, b.g + f, b.b + f, b.a);
		Color colBorder;
		if (!enabled) {
			f = 0;
		} else if (activated) {
			f = 0.2f;
		} else {
			f = -0.2f;
		}
		colBorder = new Color(b.r + f, b.g + f, b.b + f, b.a);
		Color colCheck;
		if (enabled) {
			colCheck = Color.white;
		} else {
			colCheck = new Color(0.5f, 0.5f, 0.5f, 1.0f);
		}
		
		g.drawRectangle(x, y, width, height, colFill1, colFill1, colFill2, colFill2);
		
		// Draw outer border
		g.drawRectangle(x, y, width, 1, colBorder);
		g.drawRectangle(x, y, 1, height,colBorder);
		g.drawRectangle(x, y + height -1 , width, 1, colBorder);
		g.drawRectangle(x + width - 1, y, 1, height, colBorder);
		
		if (fnt != null && text != null) {
			int decal = 0;
			if (!selectable) {
				decal = -height + 4;
			}
			fnt.setAlign(ADXFont.H_ALIGN_LEFT, ADXFont.V_ALIGN_MIDDLE);
			g.drawText(fnt, text, x + height + decal + 1, y + height / 2 + 1, Color.black);
			g.drawText(fnt, text, x + height + decal, y + height / 2, Color.white);
		}
		
		if (selectable && activated) {
			g.drawRectangle(x + 8, y + 8, height - 16, 1, Color.white);
			g.drawRectangle(x + 8, y + height - 9, height - 16, 1, Color.white);
			g.drawRectangle(x + 8, y + 8, 1, height - 16, Color.white);
			g.drawRectangle(x + height - 9, y + 8, 1, height - 16, Color.white);
			g.drawRectangle(x + 8, y + 8, height - 16, height - 16, new Color(colCheck.r, colCheck.g, colCheck.b, colCheck.a - 0.5f));
		}
		
	}
	
}
