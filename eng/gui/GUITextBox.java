package gui;

import io.ADXInput;

import org.newdawn.slick.Color;

import render.ADXFont;
import render.ADXGraphics;
import frame.ADXGame;

public class GUITextBox extends GUI {
	
	private boolean editing = false;
	
	// 
	private ADXFont fnt = GUI.defaultFont;
	private Color b = GUI.defaultColor;
	//private Color b = new Color(0.66f, 0.0f, 0.0f, 1.0f);
	private String placeHolder;
	private String text = "";
	private int keyIndex1 = 0;
	private int keyIndex2 = 0;
	private boolean override = false;
	private boolean password = false;
	
	// Timer variables
	private int hoverTimer = 0;
	private int downTimer = 0;
	private int editTimer = 0;
	private int clickTimer = 0;
	private int selectTimer = 0;

	public GUITextBox(int x, int y, int width, int height, String placeHolder, boolean password, int roomID) {
		super(x, y, width, height, roomID);
		this.placeHolder = placeHolder;
		this.password = password;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
		if (editing) {
			override = true;
		}
	}
	
	public boolean isEditing() {
		return editing;
	}
	
	public void setEditing(boolean e) {
		editing = e;
	}
	
	@Override
	public void init(ADXGame game) {
	}
	
	@Override
	public void update(ADXGame game, ADXInput input) {
		
		super.update(game, input);
		
		if (!enabled) {
			hoverTimer = 0;
			downTimer = 0;
			return;
		}
		
		if (hovered && hoverTimer < 10) {
			hoverTimer++;
		} else if (!hovered && hoverTimer > 0) {
			hoverTimer--;
		}
		
		if (mouseDown && downTimer < 5) {
			downTimer++;
		} else if (!mouseDown && downTimer > 0) {
			downTimer--;
		}
		
		if (clicked && !editing) {
			clickTimer = 25;
		} else if (clickTimer > 0) {
			clickTimer--;
		}
		
		if (selected) {
			selectTimer++;
		} else {
			selectTimer = 0;
			editing = false;
		}
		
		if (!editing && clicked) {
			input.setKeyString(text);
			input.setKeyStringIndex(text.length());
			input.setKeyStringIndex2(text.length());
			editing = true;
		}
		
		if (editing) {
			editTimer++;
			if (!override) {
				text = input.getKeyString();
			} else {
				input.setKeyString(text);
				input.setKeyStringIndex(text.length());
				input.setKeyStringIndex2(text.length());
				override = false;
			}
			keyIndex1 = input.getKeyStringIndex();
			keyIndex2 = input.getKeyStringIndex2();
		} else {
			editTimer = 0;
		}
		
	}
	
	@Override
	public void render(ADXGame game, ADXGraphics g) {
		
		float f;
		if (!enabled) {
			f = 0.0f;
		} else if (!editing) {
			f = 0.0f + hoverTimer / 60.0f + downTimer / 30.0f;
		} else {
			f = -0.2f - clickTimer / 60.0f;
		}
		Color colFill = new Color(b.r + f, b.g + f, b.b + f, b.a);
		f -= 0.2f;
		Color colFill2 = new Color(b.r + f, b.g + f, b.b + f, b.a);
		if (!enabled) {
			f = 0.1f;
		} else if (selected) {
			f = 0.3f;
		} else {
			f = 0.2f;
		}
		Color colInBorder = new Color(b.r + f, b.g + f, b.b + f, b.a);
		if (!enabled) {
			f -= 0.1f;
		} else if (selected) {
			f = (float)Math.sin(selectTimer / 15.0f) / 2.0f + 0.3f;
		} else {
			f -= 0.8f;
		}
		Color colOutBorder = new Color(b.r + f, b.g + f, b.b + f, b.a);
		Color colText;
		if (enabled) {
			colText = Color.white;
		} else {
			colText = new Color(1.0f, 1.0f, 1.0f, 0.5f);
		}
		
		// Draw fill
		g.drawRectangle(x + 2, y + 2, width - 4, height - 4, colFill, colFill, colFill2, colFill2);
		
		// Draw inner border
		g.drawRectangle(x + 1, y + 1, width - 2, 1, colInBorder);
		g.drawRectangle(x + 1, y + 1, 1, height - 2, colInBorder);
		g.drawRectangle(x + 1, y + height - 2, width - 2, 1, colInBorder);
		g.drawRectangle(x + width - 2, y + 2, 1, height - 2, colInBorder);
		
		// Draw outer border
		g.drawRectangle(x, y, width, 1, colOutBorder);
		g.drawRectangle(x, y, 1, height,colOutBorder);
		g.drawRectangle(x, y + height -1 , width, 1, colOutBorder);
		g.drawRectangle(x + width - 1, y, 1, height, colOutBorder);
		
		if (fnt != null) {
			
			// Draw text
			fnt.setAlign(ADXFont.H_ALIGN_LEFT, ADXFont.V_ALIGN_MIDDLE);
			String text = this.text;
			if (password) {
				text = text.replaceAll(".", "*");
			}
			if (text.length() > 0) {
				g.drawText(fnt, text, x + 6 + 1, y + height / 2 + 1, Color.black);
				g.drawText(fnt, text, x + 6, y + height / 2, colText);
			} else {
				g.drawText(fnt, placeHolder, x + 6, y + height / 2, new Color(1.0f, 1.0f, 1.0f, 0.4f));
			}
			
			// Draw edit mode cursor and selection range
			if (editing) {
				int xCursor = x + 6 + fnt.getWidth(text.substring(0, keyIndex1));
				if (keyIndex1 != keyIndex2) {
					int xCursor2 = x + 6 + fnt.getWidth(text.substring(0, keyIndex2));
					g.drawRectangle(xCursor, y + 4, xCursor2 - xCursor, height - 8, new Color(1.0f, 1.0f, 1.0f, 0.33f));
				}
				g.drawRectangle(xCursor, y + 4, 1, height - 8, new Color(1.0f, 1.0f, 1.0f, (float)Math.sin(editTimer / 10.0f) + 1));
			}
			
		}
		
	}

}
