package web;

public abstract class ADXWebFile {
	
	public long lastModified = 0;
	
	public abstract void write();
	
	public long getLastModified() {
		return lastModified;
	}

}
