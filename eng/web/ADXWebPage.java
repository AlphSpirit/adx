package web;

import java.util.HashMap;

import web.ADXWebServer.WebOnline;
import net.ADXNetServer;

public abstract class ADXWebPage {
	
	public void write(String text) {
		ADXNetServer.writeRawString(text);
	}
	
	public void writeLine(String line) {
		ADXNetServer.writeRawString(line + "\r\n");
	}
	
	public String replaceVariable(String text, String variable, Object content) {
		return text.replace("<!--ADX:" + variable + "-->", content.toString());
	}
	
	public abstract void write(int index, HashMap<String, String> post, WebOnline o);

}
