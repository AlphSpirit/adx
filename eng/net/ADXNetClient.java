package net;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;

import frame.ADXGame;

/**
 * Networking client class.
 * Manages the connection to the server and sending and receiving packets.
 * @author Alexandre Desbiens
 */
public abstract class ADXNetClient {

	private static boolean opened = false;
	private static Server server;
	private static byte[] buffer = new byte[0];
	private static long bIn = 0;
	private static long bOut = 0;
	
	/**
	 * Returns the number of bytes read from the server.
	 * @return Bytes received
	 */
	public static long getBytesIn() {
		return bIn;
	}
	
	/**
	 * Returns the number of bytes written to the server.
	 * @return Bytes sent
	 */
	public static long getBytesOut() {
		return bOut;
	}

	/**
	 * Opens a socket to the given server location
	 * @param ip Server IP address
	 * @param port Server port
	 */
	public static void openSocket(String ip, int port) {
		if (!opened) {
			server = new Server(ip, port);
			if (server.opened) {
				new Thread(server).start();
				opened = true;
			}
		}
	}

	/** Closes the connection with the server. */
	public static void closeSocket() {
		if (opened) {
			ADXGame.postString("Closing Client connexion...");
			if (server != null) {
				server.run = false;
			}
			opened = false;
		}
	}

	/**
	 * Tests if the socket is opened.
	 * @return Socket opened flag
	 */
	public static boolean isSocketOpened() {
		return opened;
	}

	/**
	 * Returns the underlying Server object that can read and write packets.
	 * @return Server object
	 */
	public static Server getServer() {
		if (opened && server.opened) {
			return server;
		} else {
			return null;
		}
	}

	/** Clears the packet buffer. */
	public static void clearBuffer() {
		buffer = new byte[0];
	}

	/**
	 * Writes a single byte of information to the packet buffer.
	 * @param b Byte information
	 */
	public static void writeByte(byte b) {
		byte[] bytes = new byte[1];
		bytes[0] = b;
		writeToBuffer(bytes);
		bOut += 1;
	}

	/**
	 * Writes an integer (4 bytes) to the packet buffer.
	 * @param i Integer information
	 */
	public static void writeInt(int i) {
		byte[] bytes = new byte[4];
		ByteBuffer.wrap(bytes).putInt(i);
		writeToBuffer(bytes);
		bOut += 4;
	}
	
	/**
	 * Writes a double word (8 bytes) to the packet buffer.
	 * @param d Double information
	 */
	public static void writeDouble(double d) {
		byte[] bytes = new byte[8];
		ByteBuffer.wrap(bytes).putDouble(d);
		writeToBuffer(bytes);
		bOut += 8;
	}
	
	/**
	 * Writes a string of text (4 bytes + string length) to the packet buffer.
	 * @param s String information
	 */
	public static void writeString(String s) {
		byte[] bytes = new byte[4];
		ByteBuffer.wrap(bytes).putInt(s.length());
		writeToBuffer(bytes);
		writeToBuffer(s.getBytes());
		bOut += 4 + s.length();
	}

	/**
	 * Writes the given byte array to the packet buffer.
	 * @param bytes Byte array
	 */
	private static void writeToBuffer(byte[] bytes) {
		int totalLength = buffer.length + bytes.length;
		byte[] bufferNew = new byte[totalLength];
		for (int j = 0; j < totalLength; j++) {
			if (j < totalLength - bytes.length) {
				bufferNew[j] = buffer[j];
			} else {
				bufferNew[j] = bytes[j - (totalLength - bytes.length)];
			}
		}
		buffer = bufferNew;
	}

	/** Sends the packet buffer to the server. */
	public static void send() {
		if (opened && buffer.length != 0) {
			server.send(buffer);
		}
	}

	/**
	 * Server communication class.
	 * Handles writing and reading informations on the socket.
	 * @author Alexandre Desbiens
	 */
	public static class Server implements Runnable {

		private boolean run = true;
		private boolean opened = false;

		private static Socket socket;
		private static DataInputStream dataIS;
		private static BufferedOutputStream dataOS;

		/**
		 * Opens a connection to the given server.
		 * @param ip Server IP address
		 * @param port Server port
		 */
		public Server(String ip, int port) {
			ADXGame.postString("Connecting to IP " + ip + ", Port " + port + "...");
			try {
				socket = new Socket(ip, port);
				dataIS = new DataInputStream(socket.getInputStream());
				dataOS = new BufferedOutputStream(socket.getOutputStream());
				opened = true;
			} catch (Exception e) {
				ADXGame.postWarning("Error while connecting to IP " + ip + ", Port " + port + ".");
			}
			ADXGame.postString("Connected to IP " + ip + ", Port " + port + ".");
		}

		/** Starts the connection on a separate thread. */
		public void run() {
			while (run) {
				System.out.print("");
			}
			try {
				if (dataIS != null) {
					dataIS.close();
				}
				if (dataOS != null) {
					dataOS.close();
				}
				if (socket != null) {
					socket.close();
				}
			} catch (IOException e) {
				//e.printStackTrace();
			}
			ADXGame.postString("Client closed");
		}

		/**
		 * Sends the given byte array buffer to the server.
		 * @param b Byte array
		 */
		public void send(byte[] b) {
			try {
				if (dataIS != null) {
					dataOS.write(b);
					dataOS.flush();
				}
			} catch (IOException e) {
				ADXNetClient.closeSocket();
			}
		}

		/**
		 * Reads a single byte of information on the connection.
		 * @return Byte information, or 0 if any error occurred
		 */
		public byte readByte() {
			try {
				if (dataIS.available() <= 0) {
					return 0;
				}
				byte[] bytes = new byte[1];
				if (dataIS.read(bytes) != 1) {
					return 0;
				}
				bIn += 1;
				return bytes[0];
			} catch (IOException e) {
				ADXNetClient.closeSocket();
				return 0;
			}
		}

		/**
		 * Reads an integer (4 bytes) of information on the socket.
		 * @return Integer information, or 0 if any error occurred
		 */
		public int readInt() {
			try {
				if (dataIS.available() <= 0) {
					return 0;
				}
				byte[] bytes = new byte[4];
				if (dataIS.read(bytes) != 4) {
					return 0;
				}
				bIn += 4;
				return ByteBuffer.wrap(bytes).getInt();
			} catch (IOException e) {
				ADXNetClient.closeSocket();
				return 0;
			}
		}

		/**
		 * Reads a double word (8 bytes) of information on the socket.
		 * @return Double information, or 0 if any error occurred
		 */
		public double readDouble() {
			try {
				if (dataIS.available() <= 0) {
					return 0;
				}
				byte[] bytes = new byte[8];
				if (dataIS.read(bytes) != 8) {
					return 0;
				}
				bIn += 8;
				return ByteBuffer.wrap(bytes).getDouble();
			} catch (IOException e) {
				ADXNetClient.closeSocket();
				return 0;
			}
		}
		
		/**
		 * Reads a characters string (4 bytes + string length) from the server.
		 * @return String information, or an empty string if any error occurred
		 */
		public String readString() {
			try {
				if (dataIS.available() <= 0) {
					return "";
				}
				byte[] bytes = new byte[4];
				if (dataIS.read(bytes) != 4) {
					return "";
				}
				int length = ByteBuffer.wrap(bytes).getInt();
				String str = "";
				for (int i = 0; i < length; i++) {
					str += (char) dataIS.read();
				}
				bIn += 4 + str.length();
				return str;
			} catch (IOException e) {
				ADXNetClient.closeSocket();
				return "";
			}
		}
		
	}

}