package net;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;

import frame.ADXGame;

/**
 * Networking server class.
 * Manages connections from multiple clients as well as reading and writing information to all of them.
 * @author Alexandre Desbiens
 */
public abstract class ADXNetServer {

	private static boolean opened = false;
	private static int maxClients;
	private static Client[] clients;
	private static Accept accept;
	private static Thread tAccept;
	private static byte[] buffer = new byte[0];
	
	/**
	 * Returns the number of bytes read from the given client index.
	 * @return Bytes received
	 */
	public static long getBytesIn(int client) {
		if (client < 0 || client >= maxClients || clients[client] == null) {
			return -1;
		}
		return clients[client].getBytesIn();
	}
	
	/**
	 * Returns the number of bytes written to the given client index.
	 * @return Bytes sent
	 */
	public static long getBytesOut(int client) {
		if (client < 0 || client >= maxClients || clients[client] == null) {
			return -1;
		}
		return clients[client].getBytesOut();
	}

	/**
	 * Open a given port to receive connections from multiple clients.
	 * @param port Port to open
	 * @param maxC Maximum clients
	 */
	public static void createSocket(int port, int maxC) {
		if (!opened) {
			maxClients = maxC;
			clients = new Client[maxClients];
			accept = new Accept(port);
			tAccept = new Thread(accept);
			tAccept.start();
			opened = true;
		}
	}

	/** Closes the server and all the client connections */
	public static void closeSocket() {
		if (opened) {
			accept.run = false;
			for (int i = 0; i < maxClients; i++) {
				if (clients[i] != null) {
					clients[i].run = false;
					clients[i] = null;
				}
			}
			opened = false;
			tAccept = null;
		}
	}
	
	/**
	 * Tests if the socket is opened.
	 * @return Socket opened flag
	 */
	public static boolean isSocketOpened() {
		return opened;
	}

	/**
	 * Retrieves the given client object for reading and writing packets.
	 * @param index Client index
	 * @return Client object
	 */
	public static Client getClient(int index) {
		if (index < maxClients && index >= 0) {
			return clients[index];
		} else {
			return null;
		}
	}

	/**
	 * Closes the specified client connection.
	 * @param index Client index
	 */
	public static void closeClient(int index) {
		if (clients[index] != null) {
			clients[index].run = false;
		}
	}

	/** Clears the packet buffet. */
	public static void clearBuffer() {
		buffer = new byte[0];
	}
	
	/**
	 * Writes a single byte of information to the packet buffer.
	 * @param b Byte information
	 */
	public static void writeByte(byte b) {
		byte[] bytes = new byte[1];
		bytes[0] = b;
		writeToBuffer(bytes);
	}

	/**
	 * Writes an integer (4 bytes) to the packet buffer.
	 * @param i Integer information
	 */
	public static void writeInt(int i) {
		byte[] bytes = new byte[4];
		ByteBuffer.wrap(bytes).putInt(i);
		writeToBuffer(bytes);
	}
	
	/**
	 * Writes a double word (8 bytes) to the packet buffer.
	 * @param d Double information
	 */
	public static void writeDouble(double d) {
		byte[] bytes = new byte[8];
		ByteBuffer.wrap(bytes).putDouble(d);
		writeToBuffer(bytes);
	}

	/**
	 * Writes a string of text (4 bytes + string length) to the packet buffer.
	 * @param s String information
	 */
	public static void writeString(String s) {
		byte[] bytes = new byte[4];
		ByteBuffer.wrap(bytes).putInt(s.length());
		writeToBuffer(bytes);
		writeToBuffer(s.getBytes());
	}
	
	/**
	 * Writes a string of text without the length header (string length bytes) to the packet buffer.
	 * @param s Raw string information
	 */
	public static void writeRawString(String s) {
		writeToBuffer(s.getBytes());
	}
	
	/**
	 * Writes the given byte array to the packet buffer.
	 * @param bytes Byte array
	 */
	private static void writeToBuffer(byte[] bytes) {
		int totalLength = buffer.length + bytes.length;
		byte[] bufferNew = new byte[totalLength];
		for (int j = 0; j < totalLength; j++) {
			if (j < totalLength - bytes.length) {
				bufferNew[j] = buffer[j];
			} else {
				bufferNew[j] = bytes[j - (totalLength - bytes.length)];
			}
		}
		buffer = bufferNew;
	}

	/**
	 * Sends the packet buffer to the given client.
	 * @param i Client index
	 */
	public static void sendTo(int i) {
		if (clients[i] != null && buffer != null) {
			clients[i].send(buffer);
		}
	}

	/**
	 * Sends the packet buffer to all clients except the one specified.
	 * Put -1 as the exception to send to every client connected.
	 * @param e Client exception index
	 */
	public static void broadcast(int e) {
		for (int i = 0; i < maxClients; i++) {
			if (clients[i] != null && i != e) {
				sendTo(i);
			}
		}
	}

	/**
	 * Connection accept class.
	 * Accepts incoming connections from multiple clients and filter them.
	 * @author Alexandre Desbiens
	 */
	private static class Accept implements Runnable {

		private boolean run = true;

		private ServerSocket serverSocket = null;
		private Socket socket = null;

		/**
		 * Opens the accept thread on the given port.
		 * @param port Accept port number
		 */
		public Accept(int port) {
			ADXGame.postString("Opening Port " + port + "...");
			try {
				serverSocket = new ServerSocket(port);
				serverSocket.setSoTimeout(1000);
			} catch (IOException e) {
				e.printStackTrace();
			}
			ADXGame.postString("Port " + port + " opened.");
		}

		/** Starts the accept in a separate thread. */
		public void run() {
			while (run) {
				try {
					socket = serverSocket.accept();
					boolean added = false;
					for (int i = 0; i < maxClients; i++) {
						if (clients[i] == null) {
							Client c = new Client(socket, i);
							clients[i] = c;
							ADXGame.postString("Client " + i + " " + socket.getInetAddress().getHostAddress() + " connected.");
							added = true;
							new Thread(c).start();
							break;
						}
					}
					if (!added) {
						ADXGame.postString("Client connection rejected.");
						socket.close();
					}
				} catch (IOException e) {
				}
			}
			try {
				serverSocket.close();
				if (socket != null) {
					socket.close();
				}
			} catch (IOException e) {
			}
			ADXGame.postString("Accept closed");
		}

	}

	/**
	 * Client connection class.
	 * This class starts a connection on a separate thread and reads information coming from the client.
	 * @author Alexandre Desbiens
	 */
	public static class Client implements Runnable {

		private boolean run = true;
		private int index;

		private Socket socket = null;
		private BufferedInputStream dataIS = null;
		private BufferedOutputStream dataOS = null;
		
		private long bIn = 0;
		private long bOut = 0;
		
		/**
		 * Opens a client connection with the specified socket.
		 * @param socket Connection socket
		 * @param index Client index
		 */
		public Client(Socket socket, int index) {
			this.socket = socket;
			this.index = index;
			try {
				this.dataIS = new BufferedInputStream(socket.getInputStream());
				this.dataOS = new BufferedOutputStream(socket.getOutputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		/**
		 * Returns the number of bytes read from the client.
		 * @return Bytes received
		 */
		public long getBytesIn() {
			return bIn;
		}
		
		/**
		 * Returns the number of bytes written to the client.
		 * @return Bytes sent
		 */
		public long getBytesOut() {
			return bOut;
		}
		
		/**
		 * Returns the IP address of the connected client.
		 * @return Client IP address
		 */
		public String getIP() {
			return socket.getInetAddress().getHostAddress();
		}

		/** Starts the client connection in a separate thread. */
		public void run() {
			while (run) {
				System.out.print("");
			}
			try {
				socket.close();
				dataIS.close();
				dataOS.close();
			} catch (IOException e) {
				//e.printStackTrace();
			}
			clients[index] = null;
			ADXGame.postString("Client " + index+ " closed");
		}

		/**
		 * Sends the given byte array buffer to the client.
		 * @param b Byte array
		 */
		public void send(byte[] b) {
			try {
				dataOS.write(b);
				dataOS.flush();
				bOut += b.length;
			} catch (IOException e) {
				run = false;
			}
		}

		/**
		 * Reads a single byte of information on the connection.
		 * @return Byte information, or 0 if any error occurred
		 */
		public byte readByte() {
			try {
				if (dataIS.available() <= 0) {
					return 0;
				}
				byte[] bytes = new byte[1];
				if (dataIS.read(bytes) != 1) {
					return 0;
				}
				bIn += 1;
				return bytes[0];
			} catch (IOException e) {
				ADXNetClient.closeSocket();
				return 0;
			}
		}

		/**
		 * Reads an integer (4 bytes) of information on the socket.
		 * @return Integer information, or 0 if any error occurred
		 */
		public int readInt() {
			try {
				if (dataIS.available() <= 0) {
					return 0;
				}
				byte[] bytes = new byte[4];
				if (dataIS.read(bytes) != 4) {
					return 0;
				}
				bIn += 4;
				return ByteBuffer.wrap(bytes).getInt();
			} catch (IOException e) {
				ADXNetClient.closeSocket();
				return 0;
			}
		}

		/**
		 * Reads a double word (8 bytes) of information on the socket.
		 * @return Double information, or 0 if any error occurred
		 */
		public double readDouble() {
			try {
				if (dataIS.available() <= 0) {
					return 0;
				}
				byte[] bytes = new byte[8];
				if (dataIS.read(bytes) != 8) {
					return 0.0;
				}
				bIn += 8;
				return ByteBuffer.wrap(bytes).getDouble();
			} catch (IOException e) {
				ADXNetClient.closeSocket();
				return 0.0;
			}
		}

		/**
		 * Reads a characters string (4 bytes + string length) from the server.
		 * @return String information, or an empty string if any error occurred
		 */
		public String readString() {
			try {
				if (dataIS.available() <= 0) {
					return "";
				}
				byte[] bytes = new byte[4];
				if (dataIS.read(bytes) != 4) {
					return "";
				}
				int length = ByteBuffer.wrap(bytes).getInt();
				String str = "";
				for (int i = 0; i < length; i++) {
					str += (char) dataIS.read();
				}
				bIn += 4 + str.length();
				return str;
			} catch (IOException e) {
				ADXNetClient.closeSocket();
				return "";
			}
		}
		
		/**
		 * Reads a characters string without the length header (string length bytes) from the server.
		 * @return String information, or an empty string if any error occurred
		 */
		public String readRawString() {
			try {
				if (dataIS.available() <= 0) {
					return "";
				}
				String str = "";
				while (dataIS.available() > 0) {
					str += (char) dataIS.read();
				}
				bIn += str.length();
				return str;
			} catch (IOException e) {
				ADXNetClient.closeSocket();
				return "";
			}
		}

	}

}