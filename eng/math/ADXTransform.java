package math;

import frame.ADXGame;

/**
 * Coordinates transformation class.
 * Takes an array of 2D points as argument and apply various transformations on them.
 * Currently supported transformations:
 * -Translation
 * -Scaling
 * -Rotation
 * @author adesbiens
 *
 */
public class ADXTransform {

	/** Translation */
	public static final byte T_TRANSLATE = 1;
	/** Scaling */
	public static final byte T_SCALE = 2;
	/** Rotation */
	public static final byte T_ROTATE = 3;
	/** Scaling from a point */
	public static final byte T_SCALE_FROM = 4;
	/** Rotation from a point */
	public static final byte T_ROTATE_FROM = 5;

	private int length;
	private float[] x;
	private float[] y;

	/**
	 * Initializes the object with a list of floats containing X and Y positions.
	 * X and Y positions must alternate. Ex: x1, y1, x2, y2, ...
	 * @param p X and Y positions
	 */
	public ADXTransform(float... p) {
		set(p);
	}

	/**
	 * Initializes the object with 2 arrays of X and Y positions.
	 * @param x X positions
	 * @param y Y positions
	 */
	public ADXTransform(float[] x, float[] y) {
		set(x, y);
	}

	/**
	 * Returns the X position at the given index.
	 * @param i Point index
	 * @return
	 */
	public float getX(int i) {
		return x[i];
	}
	
	/**
	 * Returns the Y position at the given index.
	 * @param i Point index
	 * @return
	 */
	public float getY(int i) {
		return y[i];
	}
	
	/**
	 * Returns the X coordinates array.
	 * @return X coordinates
	 */
	public float[] getXA() {
		return x;
	}
	
	/**
	 * Returns the Y coordinates array.
	 * @return Y coordinates
	 */
	public float[] getYA() {
		return y;
	}

	/**
	 * Sets the values of the X and Y positions to the passed list of floats.
	 * X and Y positions must alternate. Ex: x1, y1, x2, y2, ...
	 * @param p X and Y positions
	 */
	public void set(float... p) {
		if (p.length % 2 == 1) {
			return;
		}
		length = p.length / 2;
		x = new float[length];
		y = new float[length];
		for (int i = 0; i < length; i++) {
			x[i] = p[i * 2];
			y[i] = p[i * 2 + 1];
		}
	}

	/**
	 * Sets the values with new X and Y positions arrays.
	 * @param x X positions
	 * @param y Y positions
	 */
	public void set(float[] x, float[] y) {
		if (x.length != y.length) {
			return;
		}
		length = x.length;
		this.x = x.clone();
		this.y = y.clone();
	}

	/**
	 * Apply a transformation on all the points conatined in the object.
	 * @param type Transformation type
	 * @param args Arguments list
	 */
	public void applyTransformation(int type, float... args) {
		switch (type) {
		case T_TRANSLATE:
			translate(args[0], args[1]);
			break;
		case T_SCALE:
			scale(args[0], args[1]);
			break;
		case T_ROTATE:
			rotate(args[0]);
			break;
		case T_SCALE_FROM:
			scaleFrom(args[0], args[1], args[2], args[3]);
			break;
		case T_ROTATE_FROM:
			rotateFrom(args[0], args[1], args[2]);
			break;
		}
	}
	
	/**
	 * Apply multiples transformations of all the points.
	 * @param type Transformations type
	 * @param args Arguments lists
	 */
	public void applyTransformations(byte[] type, float[][] args, int number) {
		if (type.length != args.length || type.length == 0) {
			return;
		}
		for (int i = 0; i < number; i++) {
			applyTransformation(type[i], args[i]);
		}
	}

	/* Basic functions */

	/**
	 * Translates all the points by the given offset.
	 * @param x X translation
	 * @param y Y translation
	 */
	public void translate(float x, float y) {
		for (int i = 0; i < length; i++) {
			this.x[i] += x;
			this.y[i] += y;
		}
	}

	/**
	 * Scales the value with the given scales.
	 * @param x X scale (1.0f is 100%)
	 * @param y Y scale (1.0f is 100%)
	 */
	public void scale(float x, float y) {
		for (int i = 0; i < length; i++) {
			this.x[i] *= x;
			this.y[i] *= y;
		}
	}

	/**
	 * Rotates all the points in the object by the given angle.
	 * @param a Rotation angle
	 */
	public void rotate(float a) {
		float x;
		float y;
		for (int i = 0; i < length; i++) {
			x = (float) (this.x[i] * Math.cos(a) - this.y[i] * Math.sin(a));
			y = (float) (this.x[i] * Math.sin(a) + this.y[i] * Math.cos(a));
			this.x[i] = x;
			this.y[i] = y;
		}
	}

	/* Sugar functions */

	/**
	 * Scales the points from a given point.
	 * @param x Scaling origin X
	 * @param y Scaling origin Y
	 * @param xScale X scale (1.0f is 100%)
	 * @param yScale Y scale (1.0f is 100%)
	 */
	public void scaleFrom(float x, float y, float xScale, float yScale) {
		translate(-x, -y);
		scale(xScale, yScale);
		translate(x, y);
	}

	/**
	 * Rotates all the points around a given points.
	 * @param x Rotation X
	 * @param y Rotation Y
	 * @param a Rotation angle
	 */
	public void rotateFrom(float x, float y, float a) {
		translate(-x, -y);
		rotate(a);
		translate(x, y);
	}

	/** Prints all the points to the standard debug output stream. */
	public void print() {
		for (int i = 0; i < length; i++) {
			ADXGame.postString("Point " + i + ": " + x[i] + ", " + y[i]);
		}
	}

}
