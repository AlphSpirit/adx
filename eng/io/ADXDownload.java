package io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import frame.ADXGame;

/**
 * Download class that starts a file download on a separate thread.
 * @author Alexandre Desbiens
 */
public class ADXDownload implements Runnable {
	
	private String url;
	private String filePath;
	private long bytes = 0;
	private long bytesTotal = 0;
	private boolean finished = false;
	private boolean cancel = false;
	
	/**
	 * Initializes the download informations about the file to download and where to put it on disk.
	 * @param url URL of the file to download
	 * @param filePath Path of destination file
	 */
	public ADXDownload(String url, String filePath) {
		this.url = url;
		this.filePath = filePath;
	}
	
	/** Starts the download in a separate thread. */
	public void start() {
		new Thread(this).start();
	}

	@Override
	/** Starts the download in a separate thread, keeping the count of bytes read. */
	public void run() {
		try {
			URL u = new URL(url);
			URLConnection conn = u.openConnection();
			bytesTotal = conn.getContentLengthLong();
			InputStream is = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			ADXTextFile file = new ADXTextFile(filePath);
			file.clearLines();
			String line;
			while ((line = br.readLine()) != null) {
				bytes += line.length() + 2;
				file.insertLine(line);
				if (cancel) {
					return;
				}
			}
			bytes -= 2;
			file.close();
			br.close();
			finished = true;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			finished = true;
			cancel = true;
			ADXGame.postWarning("Could not finish download.");
		}
	}
	
	/**
	 * Returns whether or not the download is finished.
	 * @return Download finished flag
	 */
	public boolean isFinished() {
		return finished;
	}
	
	/**
	 * Returns true if the download has been cancelled.
	 * @return Download cancelled flag
	 */
	public boolean isCancelled() {
		return cancel;
	}
	
	/**
	 * Returns the number of bytes read in the download process.
	 * @return Number of bytes read
	 */
	public long getBytesRead() {
		return bytes;
	}
	
	/**
	 * Returns the length, in bytes, of the file to download.
	 * @return Length of file, in bytes
	 */
	public long getTotalBytes() {
		return bytesTotal;
	}
	
	/** Cancels the current download and quits. */
	public void cancel() {
		cancel = true;
	}

}
