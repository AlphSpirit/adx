package io;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import frame.ADXGame;

/**
 * A basic code interpreter.
 * It reads code from a string and interprets it following the ADXL standard.
 * Whenever a function is called, it is passed to the abstract function
 * {@link #executeFunction(String, String, String[]) executeFunction} for the user to handle.
 * The string returned replaces the function call in the code afterwards.
 * Current features implemented:
 * -Variable declaration and assignation
 * -Conditions
 * -Loops
 * -Functions and nested functions
 * -Arythmetic (does not follow operation priority yet)
 * @author Alexandre Desbiens
 */
public abstract class ADXInterpreter {
	
	private HashMap<String, Variable> mapVar = new HashMap<String, Variable>();

	private int cursor = 0;
	private String[] lines;
	private int level = 0;
	private int levelBlock = -1;
	private boolean ended = false;
	private List<Looping> lLoop = new ArrayList<Looping>();

	/**
	 * Variable container class.
	 * @author Alexandre Desbiens
	 */
	private class Variable {
		int level;
		String value;

		public Variable(int level, String value) {
			this.level = level;
			this.value = value;
		}
	}

	/**
	 * Looping index buffer class.
	 * @author Alexandre Desbiens
	 */
	private class Looping {
		int index;
		int level;

		public Looping(int index, int level) {
			this.index = index;
			this.level = level;
		}
	}

	/**
	 * Initializes the interpreter with the given string of code.
	 * @param code String of ADXL code
	 */
	public ADXInterpreter(String code) {
		lines = getCodeLines(code);
	}

	/**
	 * Splits the code lines from the given string and strips each line or trailing spaces, enters or tabulations.
	 * @param code String of ADXL code
	 * @return Lines of code contained in the string
	 */
	private String[] getCodeLines(String code) {
		code = code.replace("\n", "");
		code = code.replace("\r", "");
		code = code.replace("\t", "");
		code = code.replace("{", ";{;");
		code = code.replace("}", "};");
		if (!code.endsWith(";")) {
			postCodeError("Code must end with a ; character.");
			return null;
		}
		String[] split = code.split(";");
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < split.length; i++) {
			if (!split[i].isEmpty() && !split[i].startsWith("//")) {
				list.add(split[i].trim());
			}
		}
		return list.toArray(new String[0]);
	}

	/**
	 * Returns true if the code has reached the end.
	 * @return Code ended flag
	 */
	public boolean isEnded() {
		return ended;
	}

	/**
	 * Executes one line out of the code.
	 * @return True if the line executed properly, false if an error occured.
	 */
	public boolean executeLine() {

		if (cursor >= lines.length) {
			if (!ended) {
				ended = true;
				Variable v;
				String[] s = mapVar.keySet().toArray(new String[0]);
				for (String str : s) {
					v = mapVar.get(str);
					if (v.level >= 0) {
						mapVar.remove(str);
					}
				}
				return true;
			} else {
				postCodeError("Code has reached the end. Cannot continue further.");
				return false;
			}
		}
		String line = lines[cursor];
		cursor++;
		if (line.equals("{")) {
			level++;
		} else if (line.equals("}")) {
			level--;
			if (level < 0) {
				postCodeError("Too many closing brackets.");
				return false;
			}
			// Clear local variables
			Variable v;
			String[] keys = mapVar.keySet().toArray(new String[0]);
			for (String str : keys) {
				v = mapVar.get(str);
				if (v.level > level) {
					mapVar.remove(str);
				}
			}
			// Loop
			Looping l;
			for (int i = lLoop.size() - 1; i >= 0; i--) {
				l = lLoop.get(i);
				if (l.level == level) {
					cursor = l.index;
					lLoop.remove(l);
					break;
				}
			}
			if (level <= levelBlock) {
				levelBlock = -1;
			}
		} else if (levelBlock == -1) {
			String[] split = intelligentSplit(line, true);
			List<String> lSplit = new ArrayList<String>(Arrays.asList(split));
			if (searchForStop(lSplit)) {
				return false;
			}
			searchForBreak(lSplit);
			searchForVariable(lSplit);
			searchForFunction(lSplit);
			searchForArithmetic(lSplit);
			searchForAssignation(lSplit);
			searchForDeclaration(lSplit);
			searchForCondition(lSplit);
			searchForLoop(lSplit);
			if (!ended && lSplit.size() > 0) {
				postCodeError("Line has not executed entirely, text remains.");
				return false;
			}
		}

		return true;

	}

	/** Executes the entire code (can be stopped with the stop; statement) */
	public void execute() {
		while (!ended) {
			if (!executeLine()) {
				break;
			}
		}
	}

	/**
	 * Search for a function to execute in the given string split. Replaces the
	 * function code by the value returned.
	 * @param line String split to search
	 */
	private void searchForFunction(List<String> line) {

		String str;
		for (int i = 0; i < line.size(); i++) {
			str = line.get(i);
			if (str != null || str != "") {
				if (str.matches("^[a-zA-Z][a-zA-Z0-9]*\\.[a-zA-Z][a-zA-Z0-9]*\\(.*\\)$")) {
					String ret = analyseFunction(str, false);
					if (ret != null) {
						line.set(i, ret);
					} else {
						line.remove(i);
					}
				} else if (str.matches("^[a-zA-Z][a-zA-Z0-9]*\\(.*\\)$")) {
					String ret = analyseFunction(str, true);
					if (ret != null) {
						line.set(i, ret);
					} else {
						line.remove(i);
					}
				}
			}
		}

	}

	/**
	 * Analyzes a given function to extract the calling object (if any), the function and the arguments.
	 * Passes these parameters to the child class made by the user.
	 * @param line String to analyze
	 * @param small Calling object is present flag
	 * @return Return value given by the user
	 */
	private String analyseFunction(String line, boolean small) {

		String object = null;
		String function;
		String[] args;
		if (!small) {
			object = line.substring(0, line.indexOf('.'));
			line = line.substring(line.indexOf('.') + 1, line.length());
		}
		function = line.substring(0, line.indexOf('('));
		line = line.substring(line.indexOf('(') + 1, line.length() - 1);
		args = intelligentSplit(line, false);
		List<String> lArgs = new ArrayList<String>(Arrays.asList(args));
		searchForVariable(lArgs);
		searchForFunction(lArgs);
		searchForArithmetic(lArgs);
		return executeFunction(object, function, lArgs.toArray(new String[0]));

	}

	/**
	 * Analyzes the given code line for any condition that needs to be tested and executes them.
	 * @param line Line split of ADXL code
	 */
	private void searchForCondition(List<String> line) {

		boolean block = false;
		if (line.size() == 0 || !line.get(0).equals("if")) {
			return;
		}
		if (line.size() == 1) {
			postCodeError("Missing condition after if.");
			return;
		}
		// Number comparison
		if (line.size() == 2) {
			block = !doComparison(line.get(1), null, null);
			line.remove(1);
		} else if (line.size() == 4) {
			block = !doComparison(line.get(1), line.get(2), line.get(3));
			line.remove(3);
			line.remove(2);
			line.remove(1);
		} else {
			postCodeError("Bad number of argument for numerical condition.");
			return;
		}
		if (block) {
			levelBlock = level;
		}
		line.remove(0);

	}

	/**
	 * Analyzes the line of code for any variable declaration and creates them.
	 * @param line Line split of ADXL code
	 */
	private void searchForDeclaration(List<String> line) {

		if (line.size() == 0 || !line.get(0).equals("var")) {
			return;
		}
		if (line.size() == 1) {
			postCodeError("Missing variable name for declaration.");
			return;
		}
		if (!isValidVariableName(line.get(1))) {
			postCodeError("Given variable name is not valid.");
			return;
		}
		if (mapVar.containsKey(line.get(1))) {
			postCodeError("A variable with that name already exists.");
			return;
		}
		String name = line.get(1);
		if (line.size() == 2) {
			mapVar.put(name, new Variable(level, ""));
			line.remove(1);
		} else if (line.size() == 4) {
			if (line.get(2).equals("=")) {
				mapVar.put(name, new Variable(level, extractString(line.get(3))));
				line.remove(3);
				line.remove(2);
				line.remove(1);
			} else {
				postCodeError("Unknow operator for declaration. Expected =.");
				return;
			}
		} else {
			postCodeError("Bad number for arguments for declaration.");
			return;
		}
		line.remove(0);

	}

	/**
	 * Searches the line for variable names that must be changed to their respective values and executes.
	 * @param line Line split of ADXL code
	 */
	private void searchForVariable(List<String> line) {

		String name;
		int ex = 0;
		if (line.size() == 0) {
			return;
		}
		if (line.get(0).equals("var")) {
			ex = 2;
		} else if (line.get(0).equals("if") || line.get(0).equals("while") || line.get(0).equals("break") || line.get(0).equals("stop")) {
			ex = 1;
		} else if (isValidVariableName(line.get(0))) {
			ex = 1;
		}
		for (int i = 0; i < line.size(); i++) {
			name = line.get(i);
			if (isValidVariableName(name)) {
				if (ex == 0) {
					if (mapVar.containsKey(name)) {
						line.set(i, mapVar.get(name).value);
						postCodeDebug("Replaced variable " + name + " with " + line.get(i));
					} else {
						postCodeError("Found valid variable name but variable is non-existent: " + name + ".");
						return;
					}
				} else {
					ex--;
				}
			}
		}

	}

	/**
	 * Searches the line for any variable value modification and changes the variable container.
	 * @param line Line split of ADXL code
	 */
	private void searchForAssignation(List<String> line) {

		boolean ex = false;
		if (line.size() == 0) {
			return;
		}
		if (line.get(0).equals("var") || line.get(0).equals("if") || line.get(0).equals("while") || line.get(0).equals("break") || line.get(0).equals("stop")) {
			ex = true;
		}
		if (!ex && isValidVariableName(line.get(0))) {
			if (line.size() != 3) {
				postCodeError("Wrong assignation syntax.");
				return;
			}
			String name = line.get(0);
			if (!mapVar.containsKey(name)) {
				postCodeError("Cannot assign value to a non-existant variable.");
				return;
			}
			Variable v = mapVar.get(name);
			double num1 = 1;
			double num2 = 1;
			if (!line.get(1).equals("=")) {
				if (!isDecimal(line.get(2)) || !isDecimal(v.value)) {
					postCodeError("Cannot do mathematical operations on strings.");
					return;
				}
				num1 = Double.parseDouble(v.value);
				num2 = Double.parseDouble(line.get(2));
			}
			switch (line.get(1)) {
			case "=":
				v.value = extractString(line.get(2));
				break;
			case "-=":
				v.value = (num1 - num2) + "";
				break;
			case "+=":
				v.value = (num1 + num2) + "";
				break;
			case "*=":
				v.value = (num1 * num2) + "";
				break;
			case "/=":
				v.value = (num1 / num2) + "";
				break;
			default:
				postCodeError("Bad assignation operator.");
				return;
			}
			postCodeDebug("Set variable " + name + " to " + v.value + ".");
			line.remove(2);
			line.remove(1);
			line.remove(0);
		}

	}

	/**
	 * Searches the line for loops that need to be indexed.
	 * @param line Line split of ADXL code
	 */
	private void searchForLoop(List<String> line) {

		boolean loop;
		if (line.size() == 0 || !line.get(0).equals("while")) {
			return;
		}
		if (line.size() == 2) {
			loop = doComparison(line.get(1), null, null);
			line.remove(1);
			line.remove(0);
		} else if (line.size() == 4) {
			loop = doComparison(line.get(1), line.get(2), line.get(3));
			line.remove(3);
			line.remove(2);
			line.remove(1);
			line.remove(0);
		} else {
			postCodeError("Wrong syntaxe on loop.");
			return;
		}
		if (loop) {
			lLoop.add(new Looping(cursor - 1, level));
		} else {
			levelBlock = level;
		}

	}

	/**
	 * Search the break; statement and stop loops if needed.
	 * @param line Line split of ADXL code
	 */
	private void searchForBreak(List<String> line) {

		if (line.size() == 0 || !line.get(0).equals("break")) {
			return;
		}
		if (line.size() > 1) {
			postCodeError("Break instruction does not need arguments.");
			return;
		}
		if (lLoop.size() == 0) {
			postCodeError("Cannot break out of no loop.");
			return;
		}
		line.remove(0);
		lLoop.remove(lLoop.size() - 1);

	}

	/**
	 * Search for the stop; statement and return whether or not to continue the execution.
	 * @param line Line split of ADXL code
	 * @return Continue execution flag
	 */
	private boolean searchForStop(List<String> line) {
		if (line.size() == 0 || !line.get(0).equals("stop")) {
			return false;
		}
		if (line.size() > 1) {
			postCodeError("Cannot give arguments to stop command.");
			return false;
		}
		line.remove(0);
		return true;
	}
	
	/**
	 * Calculates any arithmetic it finds on numbers.
	 * Current supported operators are +, -, * and /.
	 * @param line Line split of ADXL code
	 */
	private void searchForArithmetic(List<String> line) {
		String str;
		String[] operators = {"/", "*", "-", "+"};
		String op;
		for (int j = 0; j < 4; j ++) {
			op = operators[j];
			for (int i = line.size() - 1; i >= 0; i--) {
				str = line.get(i);
				if (str.equals(op)) {
					if (i + 1 >= line.size()) {
						postCodeError("Found arithmetic operator but no right side number.");
						return;
					}
					if (i - 1 < 0) {
						postCodeError("Arithmetic operations must have a left side.");
						return;
					}
					if (!isDecimal(line.get(i - 1)) || !isDecimal(line.get(i + 1))) {
						postCodeError("Can only do arithmetic operations on valid numbers.");
						return;
					}
					double num1 = Double.parseDouble(line.get(i - 1));
					double num2 = Double.parseDouble(line.get(i + 1));
					line.remove(i + 1);
					line.remove(i);
					line.remove(i - 1);
					double result = 0;
					switch (str) {
					case "+":
						result = num1 + num2;
						break;
					case "-":
						result = num1 - num2;
						break;
					case "*":
						result = num1 * num2;
						break;
					case "/":
						result = num1 / num2;
						break;
					}
					line.add(i - 1, result + "");
				}
			}
		}
	}

	/**
	 * Compares 2 strings by looking at their value type with a given comparison operator.
	 * @param side1 Left side string
	 * @param operator Operator string
	 * @param side2 Right side string
	 * @return Comparison result
	 */
	private boolean doComparison(String side1, String operator, String side2) {
		if (operator == null && side2 == null) {
			if (isDecimal(side1)) {
				return Double.parseDouble(side1) != 0;
			} else {
				return side1 != "";
			}
		}
		if (isDecimal(side1) && !isDecimal(side2)) {
			postCodeError("Must compare a decimal number with another decimal number.");
			return false;
		}
		if (!isDecimal(side1)) {
			if (!operator.equals("=") && !operator.equals("==")) {
				postCodeError("Can only use equality operator when comparing sprites.");
				return false;
			}
			return side1.equals(side2);
		}
		double num1 = Double.parseDouble(side1);
		double num2 = Double.parseDouble(side2);
		switch (operator) {
		case "=":
		case "==":
			return num1 == num2;
		case "<":
			return num1 < num2;
		case "<=":
			return num1 <= num2;
		case ">":
			return num1 > num2;
		case ">=":
			return num1 >= num2;
		case "<>":
		case "!=":
			return num1 != num2;
		default:
			postCodeError("Wrong comparison operator.");
			return false;
		}
	}

	/**
	 * Executes the function given by the interpreter.
	 * @param object Object that called the function, or null if there was none
	 * @param function Name of the called function
	 * @param args Arguments of the function, or null of there was none
	 * @return String value returned by the function
	 */
	protected abstract String executeFunction(String object, String function, String[] args);

	/**
	 * Splits intelligently the given string on multiple split points. If these
	 * split points are located inside exceptions like strings, parentheses or
	 * square brackets, it is then ignored.
	 * @param str String to split
	 * @return Split string
	 */
	private String[] intelligentSplit(String str, boolean splitOnSpace) {

		if (str == null) {
			postCodeError("Cannot split null.");
			return null;
		}

		List<String> list = new ArrayList<String>();

		boolean inString = false;
		int inParent = 0;
		int inBracket = 0;
		int index = 0;
		char c;

		for (int i = 0; i < str.length(); i++) {
			c = str.charAt(i);
			if (c == '"') {
				inString = !inString;
			} else if (c == '(') {
				inParent++;
			} else if (c == ')') {
				inParent--;
				if (inParent < 0) {
					postCodeError("Missing opening paranthese.");
					return null;
				}
			} else if (c == '[') {
				inBracket++;
			} else if (c == ']') {
				inBracket--;
				if (inBracket < 0) {
					postCodeError("Missing opening square bracket.");
					return null;
				}
			} else if (((c == ' ' && splitOnSpace) || c == ',') && !inString && inParent == 0 && inBracket == 0) {
				String add = str.substring(index, i).trim();
				if (add.length() > 0) {
					list.add(add);
				}
				index = i + 1;
			}
		}
		String add = str.substring(index, str.length()).trim();
		if (add.length() > 0) {
			list.add(add);
		}
		if (inParent > 0) {
			postCodeError("Not enough closing parentheses.");
			return null;
		}
		if (inBracket > 0) {
			postCodeError("Not enough closing square braquets.");
			return null;
		}

		return list.toArray(new String[0]);

	}

	/**
	 * Tests if the value is an integer.
	 * @param str String to test
	 * @return True if the string contains an integer
	 */
	protected boolean isInteger(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Tests if the value is a decimal value.
	 * @param str String to test
	 * @return True if the string contains a decimal value
	 */
	protected boolean isDecimal(String str) {
		try {
			Double.parseDouble(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Tests if the value is a formatted string surrounded with double quotes.
	 * @param str String to test
	 * @return True if the string is a formatted string
	 */
	protected boolean isString(String str) {
		if (str.startsWith("\"") && str.endsWith("\"")) {
			return true;
		}
		return false;
	}

	/**
	 * Extracts the string from double quotes if there is any.
	 * @param str Value to strip
	 * @return Stripped string
	 */
	protected String extractString(String str) {
		if (str.startsWith("\"") && str.endsWith("\"")) {
			return str.substring(1, str.length() - 1);
		}
		return str;
	}

	/**
	 * Returns the validity of a variable name.
	 * @param str Variable name
	 * @return Name validity flag
	 */
	private boolean isValidVariableName(String str) {
		return str.matches("^[a-zA-Z][a-zA-Z0-9_]*$");
	}

	/**
	 * Posts a debug message to the default debug output stream.
	 * @param str Message
	 */
	private void postCodeDebug(String str) {
		boolean b = false;
		if (b) {
			ADXGame.postString("[CODE] " + str);
		}
	}

	/**
	 * Posts an error message to the default output stream and stops the execution of the code.
	 * @param str Message
	 */
	private void postCodeError(String str) {
		ADXGame.postString("[CODE] LINE " + cursor + ": " + lines[cursor - 1] + System.lineSeparator() + "[ERROR] " + str);
		ended = true;
	}

}
